import React, { Component } from 'react';
import { View, Text, Dimensions, Image, StatusBar, ActivityIndicator, ScrollView, BackHandler } from 'react-native';
import { Button, Header, Left, Right, Container, Content, Form, Input, Textarea } from 'native-base';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import Toast from 'react-native-root-toast';
import Controller from '../Controller/Controller'

export default class ContactUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DATA: '',
            page: 1,
            last_page: 0,
            isLoading: true,
            name: "",
            email: "",
            phone: "",
            message: "",

            error_name: "",
            error_email: "",
            error_phone: "",
            error_message: "",
        }
    }

    componentWillMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.componentDidMount()
            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate(back)
                    this.setState({
                        isLoading: true,
                        page: 1,
                        DATA: '',
                        last_page: 0
                    })
                    return true;
                }
                return false;
            });
        })
    }

    async componentDidMount() {
        this.GetData()
    }

    async GetData() {
        let GET_DATA = await Model.GET_DATA();
        if (GET_DATA) {
            this.setState({
                DATA: GET_DATA,
                isLoading: false,
                page: 1,
                last_page: GET_DATA.last_page
            })
        }
    }

    updateValue(text, field) {
        let return_text = Controller.updateValue(text, field)[0]
        let return_field = Controller.updateValue(text, field)[1]
        if (return_field == 'name') {
            this.setState({
                name: return_text,
                error_name: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_name: 'برجاء إدخال الاسم'
                })
            }
        }

        if (return_field == 'message') {
            this.setState({
                message: return_text,
                error_message: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_message: 'برجاء إدخال نص الرسالة'
                })
            }
        }
        if (return_field == 'email') {
            this.setState({
                email: return_text,
                error_email: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_email: 'برجاء إدخال البريد الإلكترونى'
                })
            }
        }

        if (return_field == 'phone') {
            this.setState({
                phone: return_text,
                error_phone: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_phone: 'برجاء إدخال الهاتف'
                })
            }
        }

    }

    async Submit() {
        this.setState({ loading: true })
        let response = await Controller.Submit(this.state)
        if (response) {
            response.map((item) => {
                if (item.key == 'error') {
                    this.setState({
                        loading: false
                    })
                } if (item.key == 'name') {
                    this.setState({
                        error_name: item.value,
                        loading: false
                    })
                }

                if (item.key == 'phone') {
                    this.setState({
                        error_phone: item.value,
                        loading: false
                    })
                }
                if (item.key == 'email') {
                    this.setState({
                        error_email: item.value,
                        loading: false
                    })
                }
                if (item.key == 'message') {
                    this.setState({
                        error_message: item.value,
                        loading: false
                    })
                }

                if (item.key == 'success') {
                    this.props.navigation.navigate('HomeScreen')
                    this.setState({
                        loading: false,
                        name: "",
                        email: "",
                        phone: "",
                        message: "",

                        error_name: "",
                        error_email: "",
                        error_phone: "",
                        error_message: "",
                    })
                }
            })

        }
    }



    render() {
        let back = this.props.navigation.getParam('back');
        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {

            return (
                <Container style={{ backgroundColor: "#e8e6e6" }}>
                    {/* **********************HEEADER**********************  */}
                    {this.Loading()}
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{'اتصل بنا'}</Text>
                        <Right style={{ flex: 1 }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back)
                                    this.setState({
                                        isLoading: true,
                                        page: 1,
                                        DATA: '',
                                        last_page: 0
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {/* **********************HEEADER**********************  */}

                    <ScrollView>
                        <View style={{ flexDirection: "column", marginHorizontal: 20, marginTop: 10 }}>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Bold", fontSize: 16, color: "#DE8E4C" }}> للاتصال</Text>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Regular", fontSize: 14, color: "#3b3a3a" }}>{this.state.DATA.phone}</Text>
                        </View>
                        <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 1, marginVertical: 10 }}></View>
                        <View style={{ flexDirection: "column", marginHorizontal: 20 }}>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Bold", fontSize: 16, color: "#DE8E4C" }}> الاستفسارات</Text>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Regular", fontSize: 14, color: "#3b3a3a" }}>{this.state.DATA.email}</Text>
                        </View>
                        <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 1, marginVertical: 10 }}></View>
                        <View style={{ flexDirection: "column", marginHorizontal: 20 }}>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Bold", fontSize: 16, color: "#DE8E4C" }}> العنوان</Text>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Regular", fontSize: 14, color: "#3b3a3a" }}>{this.state.DATA.address}</Text>
                        </View>
                        <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 1, marginVertical: 10 }}></View>
                        <View style={{ flexDirection: "column", marginHorizontal: 20 }}>
                            <Text style={{ textAlign: 'right', fontFamily: "Cairo-Bold", fontSize: 16, color: "#DE8E4C" }}> راسلنا</Text>

                            <View style={{
                                width: '100%', alignSelf: "center", height: 50, borderWidth: 1, borderColor: "#c4bebe", marginTop: 15,
                                backgroundColor: "#fff"
                            }}>
                                <Input value={this.state.name} onChangeText={(text) => this.updateValue(text, 'name')} placeholderTextColor='#c4bebe' style={{
                                    color: '#000', backgroundColor: "#fff", fontSize: 14, borderRadius: 10, marginRight: 10, fontFamily: "Cairo-Regular",
                                    textAlign: "right"
                                }}
                                    placeholder="الاسم" />
                            </View>
                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 10, fontSize: 12, color: 'red' }}>{this.state.error_name}</Text>


                            <View style={{
                                width: '100%', alignSelf: "center", height: 50, borderWidth: 1, borderColor: "#c4bebe", marginTop: 15,
                                backgroundColor: "#fff"
                            }}>
                                <Input value={this.state.email} onChangeText={(text) => this.updateValue(text, 'email')} placeholderTextColor='#c4bebe' style={{
                                    marginRight: 10, color: '#000', fontSize: 14, backgroundColor: "#fff", borderRadius: 10, fontFamily: "Cairo-Regular",
                                    textAlign: "right"
                                }}
                                    placeholder="البريد الالكترونى" />
                            </View>
                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 10, fontSize: 12, color: 'red' }}>{this.state.error_email}</Text>


                            <View style={{
                                width: '100%', alignSelf: "center", height: 50, borderWidth: 1, borderColor: "#c4bebe",
                                marginTop: 15, backgroundColor: "#fff"
                            }}>
                                <Input value={this.state.phone} keyboardType={'numeric'} onChangeText={(text) => this.updateValue(text, 'phone')} placeholderTextColor='#c4bebe' style={{
                                    marginRight: 10, color: '#000', fontSize: 14, backgroundColor: "#fff", borderRadius: 10, fontFamily: "Cairo-Regular",
                                    textAlign: "right"
                                }}
                                    placeholder="رقم الهاتف" />
                            </View>
                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 10, fontSize: 12, color: 'red' }}>{this.state.error_phone}</Text>


                            <View style={{
                                width: '100%', alignSelf: "center", height: 200, borderWidth: 1, borderColor: "#c4bebe",
                                marginTop: 15, backgroundColor: "#fff"
                            }}>
                                <Textarea value={this.state.message} onChangeText={(text) => this.updateValue(text, 'message')}
                                    placeholderTextColor='#c4bebe' placeholder={"محتوي الرساله"} style={{
                                        marginRight: 10, width: '100%',
                                        height: '100%', marginTop: 10, fontSize: 14, fontFamily: "Cairo-Regular", textAlign: "right"
                                    }}></Textarea>

                            </View>
                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 10, fontSize: 12, color: 'red' }}>{this.state.error_message}</Text>



                            <Button onPress={() => this.Submit()} block style={{
                                alignSelf: "center", width: "100%", marginTop: 20, borderRadius: 2, justifyContent: "center",
                                backgroundColor: "#DE8E4C", marginBottom: 20
                            }}>
                                <Text style={{ textAlign: "center", color: '#fff', fontSize: 18, fontFamily: "Cairo-Bold" }}>ارسل</Text>
                            </Button>
                        </View>
                    </ScrollView>
                </Container >
            );
        }
    }

    Loading() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size="large" color="#DE8E4C" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 1000
                }} />
            )
        }
    }

}
