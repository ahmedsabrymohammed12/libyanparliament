import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA() {
        var url = public_url + 'contact/us';
        console.log(url);

        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    static async Submit(DATA) {

        const Form_Inputs = new FormData();
        Form_Inputs.append('name', DATA.name);
        Form_Inputs.append('phone', DATA.phone);
        Form_Inputs.append('email', DATA.email);
        Form_Inputs.append('message', DATA.message);
        var url = public_url + 'contact/us';
        console.log('Form_Inputs', Form_Inputs);
        console.log('url', url);

        return fetch(url, {
            method: 'POST',
            body: Form_Inputs,
            headers: {
                // 'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }




}