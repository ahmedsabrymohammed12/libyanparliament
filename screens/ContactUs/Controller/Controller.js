import Toast from 'react-native-root-toast';
import Model from '../Model/Model'
import CommonController from '../../Common/Controller'
export default class Controller {

    static async Submit(DATA) {
        let responseArray = []
        // console.log('controoler valvalidue', valid);

        if (DATA.name == '') {
            responseArray.push({ 'key': 'name', 'value': 'برجاء إدخال الاسم' })
        }
        if (DATA.phone == '') {
            responseArray.push({ 'key': 'phone', 'value': 'برجاء إدخال الهاتف' })
        }
        if (DATA.email == '') {
            responseArray.push({ 'key': 'email', 'value': 'برجاء إدخال البريد الإلكترونى' })
        }
        if (DATA.message == '') {
            responseArray.push({ 'key': 'message', 'value': 'برجاء إدخال نص الرسالة' })
        }
        else {
            let RegisterResponse = await Model.Submit(DATA)
            console.log('RegisterResponse', RegisterResponse);
            if (RegisterResponse.email) {
                responseArray.push({ 'key': 'email', 'value': RegisterResponse.email[0] })
            } else if (RegisterResponse.name) {
                responseArray.push({ 'key': 'name', 'value': RegisterResponse.name[0] })
            } else if (RegisterResponse.phone) {
                responseArray.push({ 'key': 'phone', 'value': RegisterResponse.phone[0] })
            } else {
                Toast.show((RegisterResponse), {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0, backgroundColor: "#de8f4c",
                    textColor: "#000",
                })
                responseArray.push({ 'key': 'success', 'value': 'success' })
            }


        }
        return responseArray;
    }

    static updateValue(text, field) {
        if (field == 'name') {
            return [text, field]
        }

        if (field == 'phone') {
            return [text, field]
        }
        if (field == 'message') {
            return [text, field]
        }
        if (field == 'email') {
            return [text, field]
        }
    }

}
