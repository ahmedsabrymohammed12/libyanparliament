import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA(page) {
        var url = public_url + 'parliament/members?page=' + parseInt(page);
        console.log(url);
        
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }



    static async GET_DATA_president() {
        var url = public_url + 'parliament/members/president';
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    

}