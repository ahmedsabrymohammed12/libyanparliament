import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, Animated, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import CommonController from '../../Common/Controller'
import Model from '../Model/Model'
import Controller from '../Controller/Controller';

export default class ParliamentMembers extends Component {

  constructor(props) {
    super(props);
    this.state = {
      DATA: [],
      DATA_president: "",
      page: 1,
      last_page: 1,
      isLoading: true,
      LoadMore: false,
      scrollY: new Animated.Value(0),
    }
  }

  componentWillMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      this.componentDidMount()
      BackHandler.addEventListener('hardwareBackPress', () => {
        let back = this.props.navigation.getParam('back');
        if (back) {
          this.props.navigation.navigate(back)
          this.setState({
            isLoading: true,
            page: 1,
            DATA: [],
            last_page: 1
          })
          return true;
        }
        return false;
      });
    })
  }

  async componentDidMount() {
    this.GetData()
  }

  async GetData() {
    let GET_DATA = await Model.GET_DATA(1);
    let GET_DATA_president = await Model.GET_DATA_president();
    if (GET_DATA_president) {
      this.setState({
        DATA_president: GET_DATA_president
      })
    }
    if (GET_DATA) {
      this.setState({
        DATA: GET_DATA.data,
        isLoading: false,
        page: 1,
        last_page: GET_DATA.last_page
      })
    }
  }


  async LoadMore() {
    console.log('LoadMore');

    if (this.state.last_page > 1) {
      console.log('this.state.last_page', this.state.last_page);
      if (this.state.page < this.state.last_page) {
        console.log('this.state.page before', this.state.page);
        this.setState({
          LoadMore: true,
          page: this.state.page + 1
        })
        console.log('this.state.page after', this.state.page);
        let GET_DATA = await Model.GET_DATA(this.state.page + 1);
        if (GET_DATA) {
          this.setState({
            DATA: [...this.state.DATA, ...GET_DATA.data],
            LoadMore: false,
            last_page: GET_DATA.last_page
          })
        }
      } else {
      }
    } else {
    }
  }



  renderHome() {
    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#fff'
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content
          onScroll={
            Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
              {
                listener: event => {
                  if (Controller.isCloseToBottom(event.nativeEvent)) {
                    this.LoadMore()
                  }
                }
              }
            )
          }
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            backgroundColor: '#fff'

          }}

        >

          <View style={{ width: width, height: height / 3, marginTop: 10, justifyContent: 'center' }}>
            <Image source={{ uri: this.state.DATA_president.image }} style={{
              height: 170, alignSelf: 'center', borderRadius: 160,
              width: 185, resizeMode: 'cover'
            }} />
            <Text numberOfLines={1} style={{
              color: '#0f0f0f', alignSelf: 'center', textAlign: 'center', fontSize: 16,
              fontFamily: 'Cairo-Regular', marginVertical: 5
            }}>{this.state.DATA_president.name}</Text>

            <Text numberOfLines={1} style={{
              color: '#0f0f0f', alignSelf: 'center', textAlign: 'center', fontSize: 14,
              fontFamily: 'Cairo-Bold', marginVertical: 0
            }}>{this.state.DATA_president.title}</Text>
          </View>


          <View style={{
            width: width - 30, flexWrap: 'wrap', flexDirection: 'row-reverse', backgroundColor: '#f6f6f6', alignSelf: 'center',
            marginTop: 5, justifyContent: 'space-between'
          }}>

            {this.state.DATA.map((item, key) =>
              <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ParliamentMemberProfile', { back:'ParliamentMembers',item: item })}>
                <View style={{
                  flexDirection: 'row-reverse', height: 230, marginVertical: 10, alignSelf: 'center', width: '48%',
                  justifyContent: 'space-between'
                }}>
                  <Card style={{ height: '100%', width: "100%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Image source={{ uri: item.image }} style={{
                      width: '100%',
                      height: '65%'
                    }} />
                    <View style={{ height: '35%' }} >
                      <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 16, fontFamily: 'Cairo-Regular'
                      }}>{item.name}</Text>
                      {/* <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Regular'
                      }}>{'اسم اللجنة'}</Text> */}
                      <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Regular'
                      }}>{item.area_name}</Text>
                    </View>
                  </Card>
                </View>
              </TouchableWithoutFeedback>
            )}

          </View>


        </Content >
      )
    }
  }



  render() {
    let back = this.props.navigation.getParam('back');
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 4, fontFamily: 'Cairo-Bold' }}>{'أعضاء مجلس النواب'}</Text>
          <Right style={{ flex: 3, marginLeft: 10, flexDirection: 'row' }}>
            <Button transparent
              onPress={() => {
                this.props.navigation.navigate(back)
                this.setState({
                  isLoading: true,
                  page: 1,
                  DATA: [],
                  last_page: 0
                })
              }}
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/iconsBack.png')} style={{
                width: "100%",
                height: "100%"
              }} />
            </Button>
            <Button transparent
              onPress={() => this.props.navigation.navigate('Filter', { back: 'ParliamentMembers' })}
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/filter.png')} style={{
                width: "100%", tintColor: '#fff', resizeMode: 'contain',
                height: "100%"
              }} />
            </Button>
          </Right>
        </View>

        {this.renderHome()}
        {this.Loading()}
      </Container >
    );
  }

  Loading() {
    if (this.state.LoadMore) {
      return (
        <ActivityIndicator size={60} color="#de8f4c" style={{
          alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 1000
        }} />
      )
    }
  }

}
