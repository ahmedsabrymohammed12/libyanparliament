import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, ImageBackground, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import CommonController from '../../Common/Controller'
import Slideshow from 'react-native-image-slider-show';
const public_url = CommonController.getPublicUrl()
import { CachedImage } from 'react-native-cached-image';
import * as Animatable from 'react-native-animatable';

export default class GalleryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DATA: '',
      page: 1,
      ModalOrderVisible: false,
      last_page: 0,
      isLoading: true,
      image: ''
    }
  }

  componentWillMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {

      this.componentDidMount()
      BackHandler.addEventListener('hardwareBackPress', () => {
        this.props.navigation.navigate('Gallery')
        this.setState({
          isLoading: true,
          page: 1,
          DATA: '',
          last_page: 0
        })
        return true;
      });

    })
  }

  setModalOrderVisible(visible) {
    this.setState({ ModalOrderVisible: visible });
  }

  async componentDidMount() {
    let item_id = this.props.navigation.getParam('item_id');
    let GET_DATA = await Model.GET_DATA(item_id);
    if (GET_DATA) {
      this.setState({
        DATA: GET_DATA,
        isLoading: false,
        page: 1,
        last_page: GET_DATA.last_page
      })
    }
  }





  renderHome() {
    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#fff'
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content showsVerticalScrollIndicator={false} contentContainerStyle={{
          backgroundColor: '#fff'

        }}>

          <View style={{
            flexDirection: 'row-reverse', height: height / 2 - 120, alignSelf: 'center',
          }}>
            <Image source={{ uri: this.state.DATA.cover_photo }} style={{
              width: width, resizeMode: 'cover',
              height: '100%'
            }} />
          </View>

          <View style={{ marginHorizontal: 5 }} >
            <Text style={{
              color: '#000', textAlign: 'right', marginVertical: 10,
              fontSize: 16, fontFamily: 'Cairo-Bold', marginHorizontal: 5
            }}>{this.state.DATA.title}</Text>


            <View style={{ marginVertical: 5, flexDirection: 'row-reverse' }} >

              <View style={{ marginHorizontal: 5, height: 45, justifyContent: "center", backgroundColor: '#de8f4c' }}>
                <Text numberOfLines={1} style={{
                  color: '#fff', textAlign: 'right', marginHorizontal: 10,
                  fontSize: 14, fontFamily: 'Cairo-Regular'
                }}>{this.state.DATA.event_date}</Text>
              </View>

            </View>


            <View style={{
              flexDirection: 'row-reverse', flexWrap: 'wrap', backgroundColor: '#ddd', alignSelf: 'center', width: '100%',
            }}>

              {this.state.DATA.slider.map((item, key) =>
                <TouchableWithoutFeedback key={key} onPress={() => {
                  this.setState({
                    image: item
                  })
                  this.setModalOrderVisible(true)
                }}>
                  <Card style={{ marginHorizontal: 3, height: 120, width: "32%", backgroundColor: '#fff' }}>
                    <Image source={{ uri: item }} style={{
                      width: '100%',
                      height: '100%'
                    }} />
                  </Card>
                </TouchableWithoutFeedback>
              )}

            </View>

          </View>

          <Modal
            transparent={true}
            animationType="slide"
            visible={this.state.ModalOrderVisible}
          >
            <View style={{ backgroundColor: '#000', opacity: 0.6, width: '100%', height: '100%' }}>
            </View>
            <View style={{
              marginTop: '30%', width: '95%', position: 'absolute', height: height / 2 + 50, backgroundColor: '#fff', margin: 40,
              justifyContent: 'center', alignSelf: 'center', borderRadius: 10
            }}>
              <View style={{ margin: 10, justifyContent: 'center', width: '100%', height: '100%', alignSelf: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', width: '100%', height: '100%', alignSelf: 'center' }}>
                  <Image source={{ uri: this.state.image }} style={{
                    width: '100%', height: '92%', alignSelf: 'center'
                  }} />
                </View>


                <Button onPress={() => {
                  this.setModalOrderVisible(false);
                }}
                  style={{
                    marginTop: 0, borderRadius: 4, marginLeft: 5, width: '50%', justifyContent: 'center',
                    alignSelf: 'center', height: 30, backgroundColor: "#de8f4c"
                  }}>
                  <Text
                    style={{ color: "#fff", fontFamily: "Cairo-Bold", fontSize: 12, textAlign: 'center' }}
                  >{'إغلاق'}</Text></Button>

              </View>
            </View>
          </Modal>

        </Content >
      )
    }
  }



  render() {
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'تفاصيل الألبوم'}</Text>
          <Right style={{ flex: 2, marginLeft: 10, flexDirection: 'row' }}>
            <Button transparent
              onPress={() => {
                this.props.navigation.navigate('Gallery')
                this.setState({
                  isLoading: true,
                  page: 1,
                  DATA: '',
                  last_page: 0
                })
              }}
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/iconsBack.png')} style={{
                width: "100%",
                height: "100%"
              }} />
            </Button>
          </Right>
        </View>

        {this.renderHome()}

      </Container >
    );
  }

  Loading() {
    if (this.state.Loading) {
      return (
        <ActivityIndicator size={60} color="#ffcd57" style={{
          alignSelf: 'center', position: 'absolute', top: height / 1.8, zIndex: 1000
        }} />
      )
    }
  }

}
