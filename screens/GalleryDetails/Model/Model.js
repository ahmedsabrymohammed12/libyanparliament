import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA(item_id) {

        var url = public_url + 'gallery/detail/' + item_id;
        console.log(url);

        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


}