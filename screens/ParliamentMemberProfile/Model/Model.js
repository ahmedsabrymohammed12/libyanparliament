import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA(DATA) {
        var url = public_url + 'home';
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + DATA
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    static async Send_Review(order_id, Rate, FeedBack, token) {

        let lan = await AsyncStorage.getItem('lan');
        let Form_Inputs = {}
        Form_Inputs.rate = Rate,
            Form_Inputs.feedback = FeedBack
        var url = public_url + 'orders/' + order_id + '/review';
        console.log(url);
        console.log("Form_Inputs", Form_Inputs)
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(Form_Inputs),
            headers: {
                'X-localization': lan,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    static async Help(table_id, token) {
        console.log(table_id);
        console.log(token);
        let lan = await AsyncStorage.getItem('lan');
        var url = public_url + 'help-request?table_id=' + table_id;
        console.log(url);
        return fetch(url, {
            method: 'GET',
            headers: {
                'X-localization': lan,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    static async Invoice(table_id, token) {
        console.log(table_id);
        console.log(token);
        let lan = await AsyncStorage.getItem('lan');
        var url = public_url + 'invoice-request?table_id=' + table_id;
        console.log(url);
        return fetch(url, {
            method: 'GET',
            headers: {
                'X-localization': lan,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }

    static async Menu(table_id, token) {
        console.log(table_id);
        console.log(token);
        let lan = await AsyncStorage.getItem('lan');
        var url = public_url + 'customer-on-table/' + table_id;
        console.log(url);
        return fetch(url, {
            method: 'GET',
            headers: {
                'X-localization': lan,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }


}