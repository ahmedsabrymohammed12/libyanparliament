import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import CommonController from '../../Common/Controller'
import Slideshow from 'react-native-image-slider-show';
const public_url = CommonController.getPublicUrl()
import { CachedImage } from 'react-native-cached-image';
import * as Animatable from 'react-native-animatable';

export default class ParliamentMemberProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {

      position: 0,
      status: 0,
      time_interval: 4000,
    };
  }


  componentWillMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      BackHandler.addEventListener('hardwareBackPress', () => {
        let back = this.props.navigation.getParam('back');
        if (back) {
          this.props.navigation.navigate(back)
          return true;
        }
        return false;

      });

    })
  }


  renderHome() {
    let item = this.props.navigation.getParam('item');
    console.log(item);

    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#000', opacity: 0.3
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#c89951', alignSelf: 'center', marginTop: height / 2,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content showsVerticalScrollIndicator={false} contentContainerStyle={{
          backgroundColor: '#fff'

        }}>

          <View style={{
            flexDirection: 'row-reverse', height: height / 2 - 50, alignSelf: 'center',
          }}>
            <Image source={{ uri: item.image }} style={{
              width: width,
              height: '100%'
            }} />
          </View>

          <View style={{ marginHorizontal: 10 }} >


            {item.name ?
              <Text numberOfLines={1} style={{
                color: '#000', textAlign: 'right', marginVertical: 5,
                fontSize: 16, fontFamily: 'Cairo-Regular'
              }}>{item.name}</Text>
              :
              <View></View>
            }


            {item.title ?
              <Text numberOfLines={1} style={{
                color: '#000', textAlign: 'right', marginVertical: 5,
                fontSize: 16, fontFamily: 'Cairo-Regular'
              }}>{item.title}</Text>
              :
              <View></View>
            }



            {item.area_name ?
              <Text numberOfLines={1} style={{
                color: '#000', textAlign: 'right', marginVertical: 5,
                fontSize: 16, fontFamily: 'Cairo-Regular'
              }}>{item.area_name}</Text>
              :
              <View></View>
            }


            {item.year ?
              <Text numberOfLines={1} style={{
                color: '#000', textAlign: 'right', marginVertical: 5,
                fontSize: 16, fontFamily: 'Cairo-Regular'
              }}>{item.year}</Text>
              :
              <View></View>
            }



            <View style={{ marginVertical: 5, flexDirection: 'row-reverse' }} >

              <TouchableWithoutFeedback onPress={() => {
                Linking.canOpenURL(item.facebook).then(supported => {
                  if (supported) {
                    Linking.openURL(item.facebook);
                  } else {

                  }
                });
              }}>
                <View style={{ marginHorizontal: 5, height: 45, width: 45, borderRadius: 30, justifyContent: "center", backgroundColor: '#de8f4c' }}>
                  <Image source={require('../../images/facebook.png')} style={{
                    width: 22, alignSelf: 'center',
                    height: 22, resizeMode: 'contain'
                  }} />
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => {
                Linking.canOpenURL(item.twitter).then(supported => {
                  if (supported) {
                    Linking.openURL(item.twitter);
                  } else {

                  }
                });
              }}>
                <View style={{ marginHorizontal: 5, height: 45, width: 45, borderRadius: 30, justifyContent: "center", backgroundColor: '#de8f4c' }}>
                  <Image source={require('../../images/twitter.png')} style={{
                    width: 22, alignSelf: 'center',
                    height: 22, resizeMode: 'contain'
                  }} />
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => {
                Linking.canOpenURL(item.youtube).then(supported => {
                  if (supported) {
                    Linking.openURL(item.youtube);
                  } else {

                  }
                });
              }}>
                <View style={{ marginHorizontal: 5, height: 45, width: 45, borderRadius: 30, justifyContent: "center", backgroundColor: '#de8f4c' }}>
                  <Image source={require('../../images/youtube.png')} style={{
                    width: 22, alignSelf: 'center',
                    height: 22, resizeMode: 'contain'
                  }} />
                </View>
              </TouchableWithoutFeedback>

            </View>

          </View>


        </Content >
      )
    }
  }



  render() {
    let back = this.props.navigation.getParam('back');
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'بيانات العضو'}</Text>
          <Right style={{ flex: 2, marginLeft: 10, flexDirection: 'row' }}>
            <Button transparent
              onPress={() => this.props.navigation.navigate(back)}
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/iconsBack.png')} style={{
                width: "100%",
                height: "100%"
              }} />
            </Button>
          </Right>
        </View>

        {this.renderHome()}

      </Container >
    );
  }

  Loading() {
    if (this.state.Loading) {
      return (
        <ActivityIndicator size={60} color="#ffcd57" style={{
          alignSelf: 'center', position: 'absolute', top: height / 1.8, zIndex: 1000
        }} />
      )
    }
  }

}
