import React, { Component } from 'react';
import { Image, View, TouchableOpacity, Dimensions, Modal, AsyncStorage, ScrollView, TouchableWithoutFeedback, StatusBar } from 'react-native';
import { Icon, Button, Text, Right, Left, Container, Content, Header } from 'native-base';
const { width, height } = Dimensions.get('window');
import Toast from 'react-native-root-toast';
import CommonController from '../Common/Controller'
export default class MenuDrawer extends React.Component {

   constructor(props) {
      super(props);
      this.state = {

      }
   }

   async CheckLogin() {
      let token = await AsyncStorage.getItem('token');
      console.log(token);
      
      if (token) {
         this.props.navigation.navigate('Tasks', { back: 'HomeScreen' })
         this.props.navigation.closeDrawer()
      } else {
         this.props.navigation.navigate('Login', { back: 'HomeScreen' })
         this.props.navigation.closeDrawer()
      }
   }

   render() {
      return (
         <Container style={{}}>
            {/* **********************HEEADER**********************  */}
            <View style={{
               backgroundColor: '#de8f4c',
               flexDirection: 'row-reverse',
               height: height / 12,
               width: width,
            }}>
               <StatusBar backgroundColor="#de8f4c" />
               <Left style={{ flex: 1 }}>
                  <Button transparent onPress={() => this.props.navigation.closeDrawer()} style={{
                     backgroundColor: 'transparent',
                     height: '100%',
                     width: '100%',
                     justifyContent: 'center',
                  }}  >
                     <Image source={require('../images/menu.png')} style={{
                        resizeMode: 'contain',
                        width: 22,
                        height: 22
                     }} />
                  </Button>
               </Left>
               <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{'القائمة'}</Text>
               <Right style={{ flex: 1 }}>
                  <Button transparent
                     onPress={() => this.props.navigation.closeDrawer()}
                     style={{
                        backgroundColor: 'transparent',
                        height: '100%',
                        width: '100%',
                        justifyContent: 'center',
                     }}  >
                     <Image source={require('../images/iconsBack.png')} style={{
                        resizeMode: 'contain',
                        width: 22,
                        height: 22
                     }} />
                  </Button>
               </Right>
            </View>
            {/* **********************HEEADER**********************  */}

            <ScrollView >
               <View style={{ flexDirection: "column", }}>
                  <Button onPress={() => {
                     this.props.navigation.navigate('HomeScreen')
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end', justifyContent: 'flex-end' }}>
                     <Text style={{
                        color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right',
                        fontSize: 14, textAlignVertical: "center"
                     }}>الرئيسيه</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'lastNews', Title: 'اخر الاخبار', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>اخر الاخبار</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'presidency', Title: 'اخبار هيئة الرئاسة', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>اخبار هيئة الرئاسة</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'statement', Title: 'التصريحات', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>التصريحات</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ParliamentMembers', { back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>أعضاء مجلس النواب</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'committees', Title: 'اخبار اللجان', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>اخبار اللجان</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('Decisions', { Title: 'القرارات', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>القرارات</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('Gallery', { back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>معرض الصور</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('VideosScreen', { back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>الفيديوهات</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'BoardNews', Title: 'ديوان مجلس النواب', back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>ديوان مجلس النواب</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => this.CheckLogin()} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}>قائمة المهام</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>

                  <Button onPress={() => {
                     this.props.navigation.navigate('ContactUs', { back: 'HomeScreen' })
                     this.props.navigation.closeDrawer()
                  }} transparent style={{ height: 60, justifyContent: 'flex-end' }}>
                     <Text style={{ color: "#666464", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14, textAlignVertical: "center" }}> اتصل بنا</Text>
                  </Button>
                  <View style={{ backgroundColor: "#c4bebe", width: "100%", height: 2 }}></View>


               </View>
            </ScrollView>
         </Container >

      )
   }
}

