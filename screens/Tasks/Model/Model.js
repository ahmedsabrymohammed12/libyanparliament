import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA() {
        let token = await AsyncStorage.getItem('token');

        var url = public_url + 'tasks/index';
        return fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }

    
    static async Update(DATA,id) {

        let token = await AsyncStorage.getItem('token');
        const Form_Inputs = new FormData();
        Form_Inputs.append('id', id);
        Form_Inputs.append('status', DATA);
       
        console.log('datadata', Form_Inputs);
        var url = public_url + 'tasks/update';

        console.log('url', url);

        return fetch(url, {
            method: 'POST',
            body: Form_Inputs,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }

        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }

}