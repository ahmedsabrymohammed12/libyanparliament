import React, { Component } from 'react';
import { View, Text, Animated, BackHandler, Image, AsyncStorage, StatusBar, Dimensions, ActivityIndicator } from 'react-native';
import { Button, Header, Left, Right, Container, Card, Icon } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window');
import { strings } from '../../i18n'
import Toast from 'react-native-root-toast';
import Model from '../Model/Model'
export default class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Tasks: [],
            loading: false,
            last_page: '',
            page: 0,
            scrollY: new Animated.Value(0),
            date: "tomorrow",
            token: "",
            isLoading: true,
            prev_orders: [],
            current_orders: [],

            check_current_orders: 1

        };
    }


    // ************************************************************************* COMPONENT WILL MOUNT ***********************************************************
    componentWillMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            // let Will_Foucs = this.props.navigation.getParam('Will_Foucs');
            // if (Will_Foucs == 1) {
            this.componentDidMount()
            // }
            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate('HomeScreen')
                    this.setState({
                        isLoading: true,
                        Tasks: []
                    })
                    return true;
                }
                return false;
            });
        })

    }

    async componentDidMount() {
        let GET_DATA = await Model.GET_DATA();
        if (GET_DATA) {
            this.setState({
                Tasks: GET_DATA.data,
                isLoading: false,

            })
        }
    }


    async Update(Status, id) {
        this.setState({ loading: true })
        let response = await Model.Update(Status, id)
        console.log('Update', response);

        if (response) {
            this.componentDidMount()
            this.setState({ loading: false })
        }
    }


    render() {
        let back = this.props.navigation.getParam('back');
        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {
            return (
                <Container style={{
                    backgroundColor: '#ddd'
                }}>
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'قائمة المهام'}</Text>
                        <Right style={{ flex: 2, marginLeft: 10, flexDirection: 'row' }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back)
                                    this.setState({
                                        isLoading: true,
                                        Tasks: []
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: 25,
                                    width: 25,
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    width: "100%",
                                    height: "100%"
                                }} />
                            </Button>
                        </Right>
                    </View>

                    {this.Loading()}

                    <ScrollView contentContainerStyle={{ flex: 1 }}>
                        {/* <ScrollableTabView
                        style={{ marginTop: 0 }}
                        initialPage={0}
                        renderTabBar={() =>
                            <ScrollableTabBar
                                style={{ borderBottomWidth: 0, }}
                                textStyle={{ fontSize: 18 }}
                                activeTextColor={'#de8f4c'}
                                inactiveTextColor={'#ddd'}
                                tabsContainerStyle={{ justifyContent: 'flex-start' }}
                                underlineStyle={{
                                    height: 0, backgroundColor: '#fff'
                                }}
                            />
                        }
                    > */}
                        {/* ------------------------------current order tab---------------------*/}
                        <View style={{ flexDirection: 'row', width: width - 50, marginVertical: 10, alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>

                            {this.state.check_current_orders == 1 ?
                                <Button onPress={() => {

                                }} style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#de8f4c', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'غدا'}</Text>
                                </Button>

                                :

                                <Button onPress={() => {
                                    this.setState({
                                        check_current_orders: 1,
                                        date: "tomorrow"
                                    })

                                }} style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#fff', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#0f0f0f', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'غدا'}</Text>
                                </Button>

                            }

                            {this.state.check_current_orders == 2 ?
                                <Button style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#de8f4c', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'اليوم'}</Text>
                                </Button>

                                :

                                <Button onPress={() => {
                                    this.setState({
                                        check_current_orders: 2,
                                        date: "today"
                                    })

                                }} style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#fff', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#0f0f0f', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'اليوم'}</Text>
                                </Button>
                            }

                            {this.state.check_current_orders == 3 ?
                                <Button onPress={() => {
                                    this.setState({
                                        check_current_orders: 3
                                    })
                                }} style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#de8f4c', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'الأمس'}</Text>
                                </Button>

                                :

                                <Button onPress={() => {
                                    this.setState({
                                        check_current_orders: 3
                                    })
                                }} style={{
                                    borderRadius: 20,
                                    height: 40,
                                    width: '32%',
                                    marginHorizontal: 5, justifyContent: 'center',
                                    backgroundColor: '#fff', alignSelf: 'center'
                                }} >
                                    <Text style={{
                                        color: '#0f0f0f', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                        fontSize: 12, fontFamily: "Cairo-Bold"
                                    }}>{'الأمس'}</Text>
                                </Button>
                            }

                        </View>

                        <ScrollView
                            style={{
                                flex: 1, fontFamily: "Cairo-Regular",
                                backgroundColor: '#ddd',
                                paddingBottom: 5
                            }}
                            // tabLabel={strings('lang.Current')}
                            onScroll={
                                Animated.event(
                                    [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                                    {
                                        listener: event => {
                                            if (this.isCloseToBottom(event.nativeEvent)) {
                                                this.loadMore(1)
                                            }
                                        }
                                    })}
                        >
                            {this.state.Tasks.map((item, key) =>
                                <View key={key} style={{
                                    flexDirection: 'row', marginHorizontal: 8, marginVertical: 5,
                                    marginTop: 1, flexWrap: 'wrap', justifyContent: 'center'
                                }}>

                                    <View>
                                        {this.state.check_current_orders == 3 && item.date == 'yesterday' ?
                                            <Card style={{
                                                backgroundColor: '#fff', width: width - 20, alignSelf: 'center',
                                                borderRadius: 20, height: null, borderColor: '#ddd'
                                            }}>

                                                <View style={{ marginHorizontal: 20, marginVertical: 5 }}>
                                                    <Text style={{ fontFamily: 'Cairo-Bold', marginVertical: 5, fontSize: 14, color: "#0f0f0f" }}>{item.title}</Text>
                                                    <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: "#0f0f0f" }}>{item.body}</Text>
                                                </View>

                                            </Card>
                                            :
                                            <View></View>}
                                    </View>

                                    <View>
                                        {this.state.check_current_orders == 2 && item.date == 'today' ?
                                            <Card style={{
                                                backgroundColor: '#fff', width: width - 20, alignSelf: 'center',
                                                borderRadius: 20, height: null, borderColor: '#ddd'
                                            }}>

                                                <View style={{ marginHorizontal: 20, marginVertical: 5 }}>
                                                    <Text style={{ fontFamily: 'Cairo-Bold', marginVertical: 5, fontSize: 14, color: "#0f0f0f" }}>{item.title}</Text>
                                                    <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: "#0f0f0f" }}>{item.body}</Text>
                                                </View>

                                                <View style={{ flexDirection: 'row-reverse', width: '50%', marginHorizontal: 20, marginBottom: 10 }}>
                                                    <Button onPress={() => {
                                                        this.Update('yesterday', item.id)
                                                    }} style={{
                                                        borderRadius: 20,
                                                        height: 30,
                                                        width: '45%',
                                                        marginHorizontal: 5, justifyContent: 'center',
                                                        backgroundColor: '#de8f4c', alignSelf: 'center'
                                                    }} >
                                                        <Text style={{
                                                            color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                                            fontSize: 12, fontFamily: "Cairo-Bold"
                                                        }}>{'تم'}</Text>
                                                    </Button>
                                                    <Button onPress={() => {
                                                        this.Update('tomorrow', item.id)
                                                    }} style={{
                                                        borderRadius: 20,
                                                        height: 30,
                                                        width: '45%',
                                                        marginHorizontal: 5, justifyContent: 'center',
                                                        backgroundColor: '#de8f4c', alignSelf: 'center'
                                                    }} >
                                                        <Text style={{
                                                            color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                                            fontSize: 12, fontFamily: "Cairo-Bold"
                                                        }}>{'تأجيل'}</Text>
                                                    </Button>

                                                </View>
                                            </Card>
                                            :
                                            <View></View>}
                                    </View>

                                    <View>
                                        {this.state.check_current_orders == 1 && item.date == 'tomorrow' ?
                                            <Card style={{
                                                backgroundColor: '#fff', width: width - 20, alignSelf: 'center',
                                                borderRadius: 20, height: null, borderColor: '#ddd'
                                            }}>

                                                <View style={{ marginHorizontal: 20, marginVertical: 5 }}>
                                                    <Text style={{ fontFamily: 'Cairo-Bold', marginVertical: 5, fontSize: 14, color: "#0f0f0f" }}>{item.title}</Text>
                                                    <Text style={{ fontFamily: 'Cairo-Regular', fontSize: 14, color: "#0f0f0f" }}>{item.body}</Text>
                                                </View>

                                                <View style={{ flexDirection: 'row-reverse', width: '25%', marginHorizontal: 20, marginBottom: 10 }}>
                                                    <Button onPress={() => {
                                                        this.Update('yesterday', item.id)
                                                    }} style={{
                                                        borderRadius: 20,
                                                        height: 30,
                                                        width: '95%',
                                                        marginHorizontal: 5, justifyContent: 'center',
                                                        backgroundColor: '#de8f4c', alignSelf: 'center'
                                                    }} >
                                                        <Text style={{
                                                            color: '#fff', alignSelf: 'center', textAlign: "center", textAlignVertical: "center",
                                                            fontSize: 12, fontFamily: "Cairo-Bold"
                                                        }}>{'تم'}</Text>
                                                    </Button>

                                                </View>
                                            </Card>
                                            :
                                            <View></View>}
                                    </View>
                                </View>
                            )}
                        </ScrollView>

                    </ScrollView>

                    {this.state.check_current_orders !== 3 ?
                        <Button onPress={() => {
                            this.props.navigation.navigate('AddTask', { date: this.state.date })
                            this.setState({
                                isLoading: true
                            })
                            console.log('date', this.state.date)
                        }
                        } style={{
                            borderRadius: 30,
                            height: 60,
                            width: 60,
                            position: "absolute", bottom: 20, left: 20, zIndex: 100,
                            backgroundColor: '#de8f4c', justifyContent: 'center'
                        }} >
                            <Icon name='add' style={{ fontSize: 40 }} />
                        </Button>
                        :
                        <View></View>}


                </Container>
            );
        }
    }
    Loading() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size="large" color="#de8f4c" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2, zIndex: 6
                }} />
            )
        }
    }
}
