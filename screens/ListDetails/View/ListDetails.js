import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, ScrollView, BackHandler, ActivityIndicator, Dimensions } from 'react-native';
import { Button, Header, Left, Right, Container, Content, Form, Input, Textarea, Card, CardItem, Thumbnail, Body, Icon } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Model from '../Model/Model'
import Slideshow from 'react-native-image-slider-show';
const { width, height } = Dimensions.get('window');
export default class ListDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DATA: '',
            images: [],
            page: 1,
            last_page: 0,
            position: 1,
            status: 0,
            time_interval: 4000,
            isLoading: true
        }
    }

    componentWillMount() {

        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {

            this.componentDidMount()
            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate(back, { loadData: 0 })
                    this.setState({
                        isLoading: true,
                        page: 1,
                        images: [],
                        DATA: '',
                        last_page: 0
                    })
                    return true;
                }
                return false;
            });

        })
    }

    async componentDidMount() {
        this.setState({
            images: [],
            position: 1,
        })
        let DATA = this.props.navigation.getParam('DATA');
        let item_id = this.props.navigation.getParam('item_id');
        let GET_DATA = await Model.GET_DATA(DATA, item_id);
        if (GET_DATA) {
            if (GET_DATA.slider.length == 0) {
                this.state.images.push({ 'url': GET_DATA.cover_photo })
            } else {
                GET_DATA.slider.map((item) => {
                    this.state.images.push({ 'url': item })
                })
            }

            this.setState({
                DATA: GET_DATA,
                isLoading: false,
                page: 1,
                last_page: GET_DATA.last_page
            })
        }
    }



    render() {
        let back = this.props.navigation.getParam('back');
        let Title = this.props.navigation.getParam('Title');
        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {

            return (
                <Container style={{}}>
                    {/* **********************HEEADER**********************  */}
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{Title}</Text>
                        <Right style={{ flex: 1 }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back, { loadData: 0 })
                                    this.setState({
                                        isLoading: true,
                                        page: 1,
                                        images: [],
                                        DATA: '',
                                        last_page: 0
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {/* **********************HEEADER**********************  */}

                    <ScrollView >
                        <Slideshow
                            indicatorSelectedColor={'#de8f4c'}
                            indicatorColor={'#ffffff'}
                            arrowSize={30}
                            width={width}
                            height={height / 4 + 60}
                            dataSource={this.state.images}
                            position={this.state.position}
                            onPositionChanged={position => {
                                this.setState({ position })
                                console.log('position', position);

                            }}
                        />

                        <View style={{ marginHorizontal: 10, justifyContent: "center", flexDirection: "column", flex: 1 }}>
                            <Text style={{ fontFamily: "Cairo-Bold", fontSize: 16, color: "#000", textAlign: "right", marginVertical: 20 }}>{this.state.DATA.title}</Text>
                            <View style={{ backgroundColor: "#de8f4c", alignSelf: 'flex-end', flexDirection: "column" }}>
                                <Text style={{ color: "#fff", fontFamily: "Cairo-Bold", textAlign: 'right', marginHorizontal: 10 }}>{this.state.DATA.date}</Text>
                            </View>
                            <Text style={{ color: "#DE8E4C", fontFamily: "Cairo-Bold", textAlign: 'right', fontSize: 14 }}></Text>
                            <View>
                                <Text style={{ color: "#000", fontFamily: "Cairo-Regular", textAlign: 'right' }}>{this.state.DATA.upper_body}</Text>
                            </View>
                            <View style={{ backgroundColor: '#de8f4c' }}>
                                {this.state.DATA.body ?
                                    <Text style={{ color: "#000", fontFamily: "Cairo-Regular", textAlign: 'right' }}>{this.state.DATA.body}</Text>
                                    :
                                    <View></View>
                                }

                                {this.state.DATA.special_text ?
                                    <Text style={{ color: "#000", fontFamily: "Cairo-Regular", textAlign: 'right' }}>{this.state.DATA.special_text}</Text>
                                    :
                                    <View></View>
                                }
                            </View>
                            <View>
                                <Text style={{ color: "#000", fontFamily: "Cairo-Regular", textAlign: 'right' }}>{this.state.DATA.lower_body}</Text>
                            </View>
                        </View>

                    </ScrollView>
                </Container >
            );
        }
    }
}

