import React, { Component } from 'react';
import { Dimensions, Image, StatusBar, AsyncStorage, I18nManager } from 'react-native';
import { Container } from "native-base";
const { width, height } = Dimensions.get('window');
import CommonController from '../../Common/Controller'
import RNRestart from 'react-native-restart';
import I18n from 'react-native-i18n';
export default class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lan: ''
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.LoadInitialState()
        }, 3000)
    }

    LoadInitialState = async () => {
        // let restart = await AsyncStorage.getItem('restart');
        // if (restart !== '1') {
        //     AsyncStorage.setItem('restart', '1')
        //     RNRestart.Restart();
        // } else {
            this.props.navigation.navigate('HomeScreen')
        // }
    }

    render() {



        return (
            <Container style={{ backgroundColor: '#a77f42', justifyContent: 'center' }}>
                <StatusBar backgroundColor="#a77f42" />
                <Image source={require('../../images/splash.jpeg')} style={{
                    width: width
                    , height: height, alignSelf: 'center'
                }}></Image>
            </Container>
        );
    }

}
