import CommonController from '../../Common/Controller'
import Model from '../Model/Model'
const { width, height } = Dimensions.get('window');
import { Dimensions } from 'react-native';

export default class Controller {

    static isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 50
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom
    }
}
