import React, { Component } from 'react';
import { View, Text, Animated, ActivityIndicator, Image, StatusBar, ScrollView, BackHandler, Dimensions } from 'react-native';
import { Button, Header, Left, Right, Container, Content, Form, Input, Textarea } from 'native-base';
import { WebView } from 'react-native-webview';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import Controller from '../Controller/Controller';

export default class VideosScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DATA: [],
            page: 1,
            last_page: 0,
            isLoading: true,
            LoadMore: false,
            scrollY: new Animated.Value(0),
        }
    }

    componentWillMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.componentDidMount()
            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate(back)
                    this.setState({
                        isLoading: true,
                        page: 1,
                        DATA: [],
                        last_page: 0
                    })
                    return true;
                }
                return false;
            });

        })
    }

    async componentDidMount() {
        let GET_DATA = await Model.GET_DATA();
        if (GET_DATA) {
            this.setState({
                DATA: GET_DATA.data,
                isLoading: false,
                page: 1,
                last_page: GET_DATA.last_page
            })
        }
    }

    async LoadMore() {
        if (this.state.last_page > 1) {
            console.log('this.state.last_page', this.state.last_page);
            if (this.state.page < this.state.last_page) {
                console.log('this.state.page before', this.state.page);
                this.setState({
                    LoadMore: true,
                    page: this.state.page + 1
                })
                console.log('this.state.page after', this.state.page);
                let GET_DATA = await Model.GET_DATA(this.state.page + 1);
                if (GET_DATA) {
                    this.setState({
                        DATA: [...this.state.DATA, ...GET_DATA.data],
                        LoadMore: false,
                        last_page: GET_DATA.last_page
                    })
                }
            } else {
            }
        } else {
        }
    }


    render() {
        let back = this.props.navigation.getParam('back');
        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {
            return (
                <Container style={{ backgroundColor: "#fff" }}>
                    {/* **********************HEEADER**********************  */}
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{'الفيديوهات'}</Text>
                        <Right style={{ flex: 1 }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back)
                                    this.setState({
                                        isLoading: true,
                                        page: 1,
                                        DATA: [],
                                        last_page: 0
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {/* **********************HEEADER**********************  */}
                    {this.Loading()}
                    <ScrollView
                        onScroll={
                            Animated.event(
                                [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                                {
                                    listener: event => {
                                        if (Controller.isCloseToBottom(event.nativeEvent)) {
                                            this.LoadMore()
                                        }
                                    }
                                }
                            )
                        }>
                        <View style={{ flexDirection: "column", justifyContent: "center" }}>

                            {this.state.DATA.map((item, key) =>
                                <WebView key={key}
                                    style={{ width: '95%', height: height / 3, alignSelf: "center", marginBottom: 5 }}
                                    javaScriptEnabled={true}
                                    domStorageEnabled={true}
                                    source={{ uri: item.link }} />
                            )}

                        </View>

                    </ScrollView>
                </Container >
            );
        }
    }
    Loading() {
        if (this.state.LoadMore) {
            return (
                <ActivityIndicator size={60} color="#de8f4c" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 100
                }} />
            )
        }
    }
}
