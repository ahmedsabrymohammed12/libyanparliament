import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async Register(DATA, date) {

        let token = await AsyncStorage.getItem('token');

        const Form_Inputs = new FormData();
        Form_Inputs.append('title', DATA.Title);
        Form_Inputs.append('body', DATA.Body);
        Form_Inputs.append('date', date);

        console.log('datadata', Form_Inputs);
        var url = public_url + 'tasks/store';

        console.log('url', url);

        return fetch(url, {
            method: 'POST',
            body: Form_Inputs,
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + token
            }

        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }




}