import Model from '../Model/Model'
import Toast from 'react-native-root-toast';
import { AsyncStorage } from 'react-native';
import { strings } from '../../i18n'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

export default class Controller {

    /////////////////////////////////////////////////// ** Function To Submit Form Data**/////////////////////////////////////////////////////////////////////
    static async Submit(DATA, date) {
        let responseArray = []
        if (DATA.Title == '') {
            responseArray.push({ 'key': 'Title', 'value': 'برجاء إدخال  عنوان  ' })
        }
        if (DATA.Body == '') {
            responseArray.push({ 'key': 'Body', 'value': 'برجاء إدخال محتوى  ' })
        }

        else {
            let LoginResponse = await Model.Register(DATA, date)
            console.log('LoginResponse', LoginResponse);
            if (LoginResponse.status == 200) {
                Toast.show('تمت الإضافة بنجاح', {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0,
                    backgroundColor: "#ffb300", textColor: "#000", fontFamily: "Cairo-Regular"
                })
                responseArray.push({ 'key': 'success', 'value': 'success' })
            }
        }
        return responseArray;
    }

    static updateValue(text, field) {
        if (field == 'Title') {
            return [text, field]
        }
        if (field == 'Body') {
            return [text, field]
        }
    }




}
