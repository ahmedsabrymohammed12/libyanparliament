import React, { Component } from 'react';
import {
    View, Image, Text, ActivityIndicator, Dimensions, StatusBar, BackHandler, AsyncStorage
} from 'react-native';
import { Icon, Content, Input, Textarea, Button, Left, Right, Container } from "native-base";
const { width, height } = Dimensions.get('window');
import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import Controller from '../Controller/Controller'
import { ScrollView } from 'react-native-gesture-handler';
import Toast from 'react-native-root-toast';

export default class AddTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ////////////////////Form////////////////
            Title: '',
            Body: '',
            Date: '',

            ////////////////////errors////////////////
            error_Title: '',
            error_Body: '',
            loading: false,

        }
    }

    async componentWillMount() {

        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            BackHandler.addEventListener('hardwareBackPress', () => {
                this.props.navigation.navigate('Tasks', { Will_Foucs: 0 })
                this.setState({
                    Date: ''
                })
                return true;
            });
        })
    }

    /////////////////////////////////////////////////// ** Send Data From Form To Controller And Set it in State **/////////////////////////////////////////////////////////////////////
    updateValue(text, field) {
        let return_text = Controller.updateValue(text, field)[0]
        let return_field = Controller.updateValue(text, field)[1]
        if (return_field == 'Title') {
            this.setState({
                Title: return_text,
                error_Title: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_Title: 'برجاء إدخال عنوان  '
                })
            }
        }
        if (return_field == 'Body') {
            this.setState({
                Body: return_text,
                error_Body: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_Body: ('برجاء إدخال  محتوى  ')
                })
            }
        }
    }
    /////////////////////////////////////////////////// ** login **/////////////////////////////////////////////////////////////////////
    async Submit() {
        let date = this.props.navigation.getParam('date');
        this.setState({ loading: true })
        let response = await Controller.Submit(this.state, date)
        if (response) {
            response.map((item) => {
                if (item.key == 'error') {
                    this.setState({
                        loading: false
                    })
                } if (item.key == 'Title') {
                    this.setState({
                        error_Title: item.value,
                        loading: false
                    })
                } if (item.key == 'Body') {
                    this.setState({
                        error_Body: item.value,
                        loading: false
                    })
                } if (item.key == 'success') {
                    this.props.navigation.navigate('Tasks')
                    this.setState({
                        Title: '',
                        Body: '',
                        Date: ''
                    })
                    this.setState({ loading: false })
                }
            })
        }
    }



    async componentDidMount() {
        let lan = await AsyncStorage.getItem('lan')
        this.setState({ name: '', email: "", password: "" })
    }

    render() {
        return (
            <ScrollView>
                <Container style={{
                    backgroundColor: '#ddd'
                }}>
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'إضافة مهام'}</Text>
                        <Right style={{ flex: 2, marginLeft: 10, flexDirection: 'row' }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate('Tasks', { Will_Foucs: 0 })
                                    this.setState({
                                        Date: ''
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: 25,
                                    width: 25,
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    width: "100%",
                                    height: "100%"
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {this.Loading()}
                    <Content contentContainerStyle={{
                        marginHorizontal: 15,
                        justifyContent: 'center',
                    }}>
                        <View style={{
                            marginTop: 20,
                            marginBottom: 5
                        }}>

                            <View style={{
                                backgroundColor: "#fff", borderColor: '#c4bebe', borderRadius: 20, marginHorizontal: 10,
                                marginTop: 0,
                                flexDirection: 'row'
                            }}>
                                <Input value={this.state.Title} onChangeText={(text) => this.updateValue(text, 'Title')}
                                    placeholder={'العنوان'} placeholderTextColor="#c4bebe"
                                    style={{
                                        textAlign: 'right', marginHorizontal: 10, width: '100%', height: '100%', fontSize: 14,
                                        fontFamily: "Cairo-Regular"
                                    }}>
                                </Input>
                            </View>
                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 20, fontSize: 12, color: 'red' }}>{this.state.error_Title}</Text>

                            <View style={{
                                width: '95%', borderRadius: 20, alignSelf: "center", height: height / 2, borderWidth: 1, borderColor: "#c4bebe",
                                marginTop: 15, backgroundColor: "#fff"
                            }}>
                                <Textarea value={this.state.Body} onChangeText={(text) => this.updateValue(text, 'Body')}
                                    placeholderTextColor='#c4bebe' placeholder={"المحتوى"} style={{
                                        marginRight: 10, width: '95%',
                                        height: '100%', marginTop: 10, fontSize: 14, fontFamily: "Cairo-Regular", textAlign: "right"
                                    }}></Textarea>
                            </View>

                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 20, fontSize: 12, color: 'red' }}>{this.state.error_Body}</Text>
                            <Button onPress={() => this.Submit()} style={{
                                borderRadius: 20,
                                marginTop: 0, width: '90%', marginTop: 10, alignSelf: 'center',
                                marginHorizontal: 5,
                                backgroundColor: '#de8f4c',
                                justifyContent: 'center'
                            }} >
                                <Text style={{
                                    color: '#fff', textAlign: 'center',
                                    fontSize: 16, fontFamily: "Cairo-Bold"
                                }}>{'اضافة'}</Text>
                            </Button>

                        </View>

                    </Content>
                </Container>
            </ScrollView>
        );
    }

    Loading() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size="large" color="#ffcd57" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 1000
                }} />
            )
        }
    }

}
