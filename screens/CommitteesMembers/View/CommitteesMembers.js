import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, ImageBackground, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import CommonController from '../../Common/Controller'
import Slideshow from 'react-native-image-slider-show';
const public_url = CommonController.getPublicUrl()
import { CachedImage } from 'react-native-cached-image';
import * as Animatable from 'react-native-animatable';

export default class CommitteesMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentWillMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      BackHandler.addEventListener('hardwareBackPress', () => {
        let back = this.props.navigation.getParam('back');
        if (back) {
          this.props.navigation.navigate(back)
          this.setState({

          })
          return true;
        }
        return false;
      });

    })
  }



  async componentDidMount() {

  }




  renderHome() {
    let DATA = this.props.navigation.getParam('DATA');

    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#000', opacity: 0.3
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#c89951', alignSelf: 'center', marginTop: height / 2,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content showsVerticalScrollIndicator={false} contentContainerStyle={{
          backgroundColor: '#fff'

        }}>

          <View style={{
            width: width - 30, flexWrap: 'wrap', flexDirection: 'row-reverse', backgroundColor: '#f6f6f6', alignSelf: 'center',
            marginTop: 5, justifyContent: 'space-between'
          }}>


            {DATA.map((item, key) =>
              <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ParliamentMemberProfile', { back: 'CommitteesMembers', item: item })}>
                <View style={{
                  flexDirection: 'row-reverse', height: 230, marginVertical: 10, alignSelf: 'center', width: '48%',
                  justifyContent: 'space-between'
                }}>
                  <Card style={{ height: '100%', width: "100%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Image source={{ uri: item.image }} style={{
                      width: '100%',
                      height: '65%'
                    }} />
                    <View style={{ height: '35%' }} >
                      <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 16, fontFamily: 'Cairo-Regular'
                      }}>{item.name}</Text>
                      {/* <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Regular'
                      }}>{'اسم اللجنة'}</Text> */}
                      <Text numberOfLines={1} style={{
                        color: '#000', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Regular'
                      }}>{item.area_name}</Text>
                    </View>
                  </Card>
                </View>
              </TouchableWithoutFeedback>
            )}

          </View>


        </Content >
      )
    }
  }



  render() {
    let back = this.props.navigation.getParam('back');
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'أعضاء اللجان'}</Text>
          <Right style={{ flex: 3, marginLeft: 10, flexDirection: 'row' }}>
            <Button transparent
              onPress={() => this.props.navigation.navigate(back)}
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/iconsBack.png')} style={{
                width: "100%",
                height: "100%"
              }} />
            </Button>

          </Right>
        </View>

        {this.renderHome()}




      </Container >
    );
  }

  Loading() {
    if (this.state.Loading) {
      return (
        <ActivityIndicator size={60} color="#ffcd57" style={{
          alignSelf: 'center', position: 'absolute', top: height / 1.8, zIndex: 1000
        }} />
      )
    }
  }

}
