import React, { Component } from 'react';
import { View, Text, Animated, ActivityIndicator, Image, StatusBar, ScrollView, BackHandler, Dimensions } from 'react-native';
import { Button, Header, Left, Right, Container, Content, Form, Input, Textarea, Card, CardItem, Thumbnail, Body, Icon } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Model from '../Model/Model'
const { width, height } = Dimensions.get('window');
import Controller from '../Controller/Controller';

export default class ListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DATA: [],
            page: 1,
            last_page: 0,
            LoadMore: false,
            scrollY: new Animated.Value(0),
            isLoading: true
        }
    }

    componentWillMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            let loadData = this.props.navigation.getParam('loadData');
            console.log('loadData', loadData);
            if (loadData) {
                this.componentDidMount()
            }

            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate(back)
                    this.setState({
                        isLoading: true,
                        page: 1,
                        DATA: [],
                        last_page: 0
                    })
                    return true;
                }
                return false;
            });
        })
    }

    async componentDidMount() {
        this.GetData()
    }

    async GetData() {
        let DATA = this.props.navigation.getParam('DATA');
        let GET_DATA = await Model.GET_DATA(DATA, 1);
        if (GET_DATA) {
            this.setState({
                DATA: GET_DATA.data,
                isLoading: false,
                page: 1,
                last_page: GET_DATA.last_page
            })
        }
    }

    async LoadMore() {
        if (this.state.last_page > 1) {
            console.log('this.state.last_page', this.state.last_page);
            if (this.state.page < this.state.last_page) {
                console.log('this.state.page before', this.state.page);
                this.setState({
                    LoadMore: true,
                    page: this.state.page + 1
                })
                console.log('this.state.page after', this.state.page);
                let DATA = this.props.navigation.getParam('DATA');

                let GET_DATA = await Model.GET_DATA(DATA, this.state.page + 1);
                if (GET_DATA) {
                    this.setState({
                        DATA: [...this.state.DATA, ...GET_DATA.data],
                        LoadMore: false,
                        last_page: GET_DATA.last_page
                    })
                }
            } else {
            }
        } else {
        }
    }

    render() {
        let back = this.props.navigation.getParam('back');
        let Title = this.props.navigation.getParam('Title');
        let DATA = this.props.navigation.getParam('DATA');

        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {
            return (
                <Container style={{}}>
                    {/* **********************HEEADER**********************  */}
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        alignItems: 'center',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            {/* <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button> */}
                        </Left>
                        <Text style={{ color: '#fff', textAlign: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{Title}</Text>
                        <Right style={{ flex: 1 }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back)
                                    this.setState({
                                        isLoading: true,
                                        page: 1,
                                        DATA: [],
                                        last_page: 0
                                    })
                                }
                                }
                                style={{
                                    backgroundColor: 'transparent',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {this.Loading()}
                    {/* **********************HEEADER**********************  */}
                    <ScrollView onScroll={
                        Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                            {
                                listener: event => {
                                    if (Controller.isCloseToBottom(event.nativeEvent)) {
                                        this.LoadMore()
                                    }
                                }
                            }
                        )
                    }>
                        <View style={{ marginHorizontal: 20, justifyContent: "center", flexDirection: "column" }}>
                            {this.state.DATA.map((item, key) =>
                                <TouchableWithoutFeedback key={key} onPress={() => {
                                    this.props.navigation.navigate("ListDetails", {
                                        DATA: DATA, item_id: item.id,
                                        back: "ListScreen"
                                    })
                                }}>
                                    <Card style={{ width: width - 30, alignSelf: 'center' }}>
                                        <CardItem cardBody>
                                            <Image source={{ uri: item.cover_photo }} style={{
                                                height: 200,
                                                width: null, flex: 1
                                            }} />
                                            <View style={{
                                                position: "absolute", bottom: 0, right: 0,
                                                backgroundColor: "#de8f4c", width: 100, height: 30
                                            }}>
                                                <Text style={{ color: "#fff", fontFamily: "Cairo-Bold", textAlign: 'center' }}>{item.date}</Text>
                                            </View>
                                        </CardItem>
                                        <CardItem style={{ flexDirection: "column", alignItems: 'flex-end' }}>
                                            <Text numberOfLines={1} style={{
                                                fontFamily: "Cairo-Bold", fontSize: 14, color: "#000",
                                            }}>{item.title}</Text>
                                            <Text numberOfLines={2} style={{
                                                fontFamily: "Cairo-Regular", fontSize: 14, color: "#3b3a3a",
                                            }}>{item.title}</Text>
                                        </CardItem>
                                    </Card>
                                </TouchableWithoutFeedback>
                            )}
                        </View>
                    </ScrollView>
                </Container >
            );
        }
    }
    Loading() {
        if (this.state.LoadMore) {
            return (
                <ActivityIndicator size={60} color="#de8f4c" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 100
                }} />
            )
        }
    }

}

