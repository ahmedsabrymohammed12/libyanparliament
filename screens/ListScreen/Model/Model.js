import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA(DATA, page) {
        if (DATA == 'lastNews') {
            var url = public_url + 'last/news/lastNews?page=' + parseInt(page);
        } else if (DATA == 'presidency') {
            var url = public_url + 'last/news/presidency?page=' + parseInt(page);
        } else if (DATA == 'committees') {
            var url = public_url + 'last/news/committees?page=' + parseInt(page);
        } else if (DATA == 'statement') {
            var url = public_url + 'last/news/statement?page=' + parseInt(page);
        } else if (DATA == 'BoardNews') {
            var url = public_url + 'last/news/BoardNews?page=' + parseInt(page);
        }
        console.log(url);

        return fetch(url, {
            method: 'GET', 
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


}