import firebase from 'react-native-firebase';
import { AsyncStorage } from 'react-native';

export default class Controller {
    static getPublicUrl() {
        return 'http://parliament.flyfoxsoftware.com/api/v1/';
    }


    static async getFcmToken() {
        return firebase.messaging().getToken().then(fcmToken => {
            console.log(fcmToken);
            return fcmToken;
        })
    }

    static async getToken() {
        let token = await AsyncStorage.getItem('token');
        return token
    }



    static getLatLang() {

    }


    static async checkActive() {
        let lan = await AsyncStorage.getItem('lan');
        let token = await AsyncStorage.getItem('token');
        var url = 'http://enjoy.flyfoxsoftware.com/api/v1/check-active';
        console.log(token);
        console.log(lan);

        return fetch(url, {
            method: 'GET',
            headers: {
                'X-localization': lan,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }

}