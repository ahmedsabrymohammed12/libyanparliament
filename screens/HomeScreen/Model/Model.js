import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA() {
        var url = public_url + 'home';
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('sponser response', response)
                return response.data
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    

}