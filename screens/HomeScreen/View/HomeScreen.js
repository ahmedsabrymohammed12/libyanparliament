import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, ImageBackground, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import CommonController from '../../Common/Controller'
import Slideshow from 'react-native-image-slider-show';
const public_url = CommonController.getPublicUrl()
import { CachedImage } from 'react-native-cached-image';
import * as Animatable from 'react-native-animatable';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      lastNews: [],
      presidency: [],
      decision: [],
      statement: [],
      position: 0,
      status: 0,
      time_interval: 4000,
      isLoading: true
    };
  }


  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.images.length - 1 ? 0 : this.state.position + 1
        });
      }, this.state.time_interval)
    });

    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      CommonController.getLatLang();
      BackHandler.addEventListener('hardwareBackPress', () => {
        BackHandler.exitApp()
        return true;
      });
    })
  }

  async componentDidMount() {

    let GET_DATA = await Model.GET_DATA();
    if (GET_DATA) {
      // console.log('GET_DATA', GET_DATA);
      let slides = []
      let FirstObject = GET_DATA.statement[0]
      let SecoundObject = GET_DATA.presidency[0]
      let ThirdObject = GET_DATA.lastNews[0]
      slides.push(FirstObject)
      slides.push(SecoundObject)
      slides.push(ThirdObject)
      // console.log('slides', slides);
      slides.map((item) => {
        this.state.images.push({ 'url': item.cover_photo, 'caption': item.title + '..', 'title': item.date })
      })
      this.setState({
        lastNews: GET_DATA.lastNews,
        presidency: GET_DATA.presidency,
        decision: GET_DATA.decision,
        statement: GET_DATA.statement,
        isLoading: false
      })
    }
  }

  renderHome() {
    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#fff'
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content showsVerticalScrollIndicator={false} contentContainerStyle={{
          backgroundColor: '#fff'

        }}>
          <Slideshow
            indicatorSelectedColor={'#DE8E4C'}
            indicatorColor={'#ffffff'}
            arrowSize={0}
            width={width}
            height={height / 4 + 20}
            captionStyle={{
              color: '#000', fontFamily: 'Cairo-Regular', fontSize: 14, textAlign: 'center', backgroundColor: '#fff',
              height: 30, width: width - 30, alignSelf: 'center', paddingHorizontal: 10
            }}
            titleStyle={{
              color: '#fff', textAlign: 'center', fontFamily: 'Cairo-Bold', fontSize: 14, backgroundColor: '#de8f4c', height: 28,
              width: width / 4 + 10, alignSelf: 'flex-end'
            }}
            dataSource={this.state.images}
            scrollEnabled={false}
            position={this.state.position}
          />


          {/* ////////////////////////////////////////// أخر الأخبار /////////////////////// */}
          <View style={{ width: width, backgroundColor: '#f6f6f6', height: 220, alignSelf: 'center', marginTop: 5 }}>

            <View style={{ flexDirection: 'row-reverse', width: '95%', marginTop: 5, height: 30, alignSelf: 'center' }}>
              <Right style={{ flex: 1 }}>
                <Text style={{ color: '#464646', alignSelf: 'flex-end', fontSize: 16, fontFamily: 'Cairo-Bold' }}>{'أخر الأخبار'}</Text>
              </Right>

              <Left style={{ flex: 1 }}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'lastNews', Title: 'أخر الأخبار', back: 'HomeScreen' })}>
                  <Text style={{ color: '#c24742', alignSelf: 'flex-start', fontSize: 14, fontFamily: 'Cairo-Bold' }}>{'شاهد المزيد'}</Text>
                </TouchableWithoutFeedback>
              </Left>
            </View>

            <View style={{ flexDirection: 'row-reverse', height: 170, alignSelf: 'center', width: '95%', justifyContent: 'space-between' }}>

              {this.state.lastNews.map((item, key) =>
                <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ListDetails',
                  { Title: 'تفاصيل الخبر', DATA: 'lastNews', item_id: item.id, back: 'HomeScreen' })}>
                  <Card style={{ height: '100%', width: "48%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Image source={{ uri: item.cover_photo }} style={{
                      width: '100%',
                      height: '70%'
                    }} />
                    <View style={{
                      backgroundColor: '#de8f4c', height: 25, justifyContent: 'center', alignItems: 'center',
                      width: '50%', alignSelf: 'flex-end', position: 'absolute', top: '55.5%'
                    }}>
                      <Text style={{
                        color: '#fff', alignSelf: 'center', textAlign: 'center', fontSize: 12, flex: 5,
                        fontFamily: 'Cairo-Bold'
                      }}>{item.date}</Text>
                    </View>
                    <View style={{ height: '30%' }} >
                      <Text numberOfLines={2} style={{
                        color: '#b1b1b1', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Bold'
                      }}>{item.title}</Text>
                    </View>
                  </Card>
                </TouchableWithoutFeedback>
              )}
            </View>

          </View>
          {/* ////////////////////////////////////////// أخر الأخبار /////////////////////// */}

          {/* ////////////////////////////////////////// هيئة الرئاسة /////////////////////// */}
          <View style={{ width: width, backgroundColor: '#f6f6f6', height: 220, alignSelf: 'center', marginTop: 10 }}>

            <View style={{ flexDirection: 'row-reverse', width: '95%', marginTop: 5, height: 30, alignSelf: 'center' }}>
              <Right style={{ flex: 1 }}>
                <Text style={{ color: '#464646', alignSelf: 'flex-end', fontSize: 16, fontFamily: 'Cairo-Bold' }}>{'هيئة الرئاسة'}</Text>
              </Right>

              <Left style={{ flex: 1 }}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'presidency', Title: 'هيئة الرئاسة', back: 'HomeScreen' })}>
                  <Text style={{ color: '#c24742', alignSelf: 'flex-start', fontSize: 14, fontFamily: 'Cairo-Bold' }}>{'شاهد المزيد'}</Text>
                </TouchableWithoutFeedback>
              </Left>
            </View>

            <View style={{ flexDirection: 'row-reverse', height: 170, alignSelf: 'center', width: '95%', justifyContent: 'space-between' }}>
              {this.state.presidency.map((item, key) =>
                <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ListDetails',
                  { Title: 'تفاصيل خبر هيئة الرئاسة', DATA: 'presidency', item_id: item.id, back: 'HomeScreen' })}>
                  <Card style={{ height: '100%', width: "48%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Image source={{ uri: item.cover_photo }} style={{
                      width: '100%',
                      height: '70%'
                    }} />
                    <View style={{
                      backgroundColor: '#de8f4c', height: 25, justifyContent: 'center', alignItems: 'center',
                      width: '50%', alignSelf: 'flex-end', position: 'absolute', top: '55.5%'
                    }}>
                      <Text style={{
                        color: '#fff', alignSelf: 'center', textAlign: 'center', fontSize: 12, flex: 5,
                        fontFamily: 'Cairo-Bold'
                      }}>{item.date}</Text>
                    </View>
                    <View style={{ height: '30%' }} >
                      <Text numberOfLines={2} style={{
                        color: '#b1b1b1', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Bold'
                      }}>{item.title}</Text>
                    </View>
                  </Card>
                </TouchableWithoutFeedback>
              )}

            </View>

          </View>
          {/* ////////////////////////////////////////// هيئة الرئاسة /////////////////////// */}

          {/* //////////////////////////////////////////  تصريحات /////////////////////// */}
          <View style={{ width: width, backgroundColor: '#f6f6f6', height: 220, alignSelf: 'center', marginTop: 10 }}>

            <View style={{ flexDirection: 'row-reverse', width: '95%', marginTop: 5, height: 30, alignSelf: 'center' }}>
              <Right style={{ flex: 1 }}>
                <Text style={{ color: '#464646', alignSelf: 'flex-end', fontSize: 16, fontFamily: 'Cairo-Bold' }}>{'تصريحات'}</Text>
              </Right>

              <Left style={{ flex: 1 }}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ListScreen', { loadData: 1, DATA: 'statement', Title: 'تصريحات', back: 'HomeScreen' })}>
                  <Text style={{ color: '#c24742', alignSelf: 'flex-start', fontSize: 14, fontFamily: 'Cairo-Bold' }}>{'شاهد المزيد'}</Text>
                </TouchableWithoutFeedback>
              </Left>
            </View>

            <View style={{ flexDirection: 'row-reverse', height: 170, alignSelf: 'center', width: '95%', justifyContent: 'space-between' }}>
              {this.state.statement.map((item, key) =>
                <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ListDetails',
                  { Title: 'تفاصيل التصريح', DATA: 'statement', item_id: item.id, back: 'HomeScreen' })}>
                  <Card style={{ height: '100%', width: "48%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <Image source={{ uri: item.cover_photo }} style={{
                      width: '100%',
                      height: '70%'
                    }} />
                    <View style={{
                      backgroundColor: '#de8f4c', height: 25, justifyContent: 'center', alignItems: 'center',
                      width: '50%', alignSelf: 'flex-end', position: 'absolute', top: '55.5%'
                    }}>
                      <Text style={{
                        color: '#fff', alignSelf: 'center', textAlign: 'center', fontSize: 12, flex: 5,
                        fontFamily: 'Cairo-Bold'
                      }}>{item.date}</Text>
                    </View>
                    <View style={{ height: '30%' }} >
                      <Text numberOfLines={2} style={{
                        color: '#b1b1b1', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Bold'
                      }}>{item.title}</Text>
                    </View>
                  </Card>
                </TouchableWithoutFeedback>
              )}

            </View>

          </View>
          {/* //////////////////////////////////////////  تصريحات /////////////////////// */}



          {/* //////////////////////////////////////////  قرارات /////////////////////// */}
          <View style={{ width: width, backgroundColor: '#f6f6f6', height: 220, alignSelf: 'center', marginTop: 10 }}>

            <View style={{ flexDirection: 'row-reverse', width: '95%', marginTop: 5, height: 30, alignSelf: 'center' }}>
              <Right style={{ flex: 1 }}>
                <Text style={{ color: '#464646', alignSelf: 'flex-end', fontSize: 16, fontFamily: 'Cairo-Bold' }}>{'قرارات'}</Text>
              </Right>

              <Left style={{ flex: 1 }}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Decisions', { DATA: 'decision', Title: 'قرارات', back: 'HomeScreen' })}>
                  <Text style={{ color: '#c24742', alignSelf: 'flex-start', fontSize: 14, fontFamily: 'Cairo-Bold' }}>{'شاهد المزيد'}</Text>
                </TouchableWithoutFeedback>
              </Left>
            </View>

            <View style={{ flexDirection: 'row-reverse', height: 170, alignSelf: 'center', width: '95%', justifyContent: 'space-between' }}>
              {this.state.decision.map((item, key) =>
                <TouchableWithoutFeedback key={key} onPress={() => this.props.navigation.navigate('ListDetails',
                  { Title: 'تفاصيل القرار', DATA: 'decision', item_id: item.id, back: 'HomeScreen' })}>
                  <Card style={{ height: '100%', width: "48%", backgroundColor: '#fff', justifyContent: 'center' }}>
                    <View style={{
                      backgroundColor: '#de8f4c', height: 25, justifyContent: 'center', alignItems: 'center',
                      width: '50%', alignSelf: 'flex-end', position: 'absolute', top: 0, left: 0
                    }}>
                      <Text style={{
                        color: '#fff', alignSelf: 'center', textAlign: 'center', fontSize: 12, flex: 5,
                        fontFamily: 'Cairo-Bold'
                      }}>{item.date}</Text>
                    </View>
                    <View style={{ height: '100%' }} >
                      <Text numberOfLines={1} style={{
                        color: '#b1b1b1', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 14, fontFamily: 'Cairo-Bold', marginVertical: 5, marginTop: 22
                      }}>{item.title}</Text>
                      <Text numberOfLines={5} style={{
                        color: '#b1b1b1', textAlign: 'right', marginHorizontal: 5,
                        fontSize: 12, fontFamily: 'Cairo-Bold'
                      }}>{item.body}</Text>
                    </View>
                  </Card>
                </TouchableWithoutFeedback>
              )}
            </View>

          </View>
          {/* //////////////////////////////////////////  قرارات /////////////////////// */}

        </Content>
      )
    }
  }



  render() {
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{'الصفحة الرئيسية'}</Text>
          <Right style={{ flex: 1 }}>

            <Button transparent
              onPress={() => this.props.navigation.navigate('Notifications', { back: 'HomeScreen' })}
              style={{
                backgroundColor: 'transparent',
                height: '100%',
                width: '100%',
                justifyContent: 'center',
              }}  >
              {/* <Image source={require('../../images/Group_356.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} /> */}
            </Button>
          </Right>
        </View>

        {this.renderHome()}




      </Container >
    );
  }

  Loading() {
    if (this.state.Loading) {
      return (
        <ActivityIndicator size={60} color="#ffcd57" style={{
          alignSelf: 'center', position: 'absolute', top: height / 1.8, zIndex: 1000
        }} />
      )
    }
  }

}
