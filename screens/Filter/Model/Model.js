import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { AsyncStorage } from 'react-native';

export default class Model {

    static async GET_DATA() {
        var url = public_url + 'committees';
        console.log(url);

        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log(' response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)
            });
    }


    static async Submit(DATA) {
        let Form_Inputs = {}
        Form_Inputs.commission_id = DATA.api_options.toString()
        var url = public_url + 'filter';
        console.log(url);
        console.log(Form_Inputs);
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(Form_Inputs),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => {
                console.log('response', response)
                return response
            })
            .catch(error => {
                console.log('error', error)

            });
    }



}