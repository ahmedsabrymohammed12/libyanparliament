import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Image, StatusBar, ScrollView, BackHandler, Dimensions } from 'react-native';
import { Button, Header, Left, Right, Container, Content, Form, Input, Textarea, Footer } from 'native-base';
const { width, height } = Dimensions.get('window');
import SelectMultiple from 'react-native-select-multiple'
import Model from '../Model/Model'

export default class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            phone: '',
            messsage: '',
            options: [],
            SelectedOptions: [],
            api_options: [],
            isLoading: true,
            loading: false
        }
    }

    componentWillMount() {
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.GetData()
            BackHandler.addEventListener('hardwareBackPress', () => {
                let back = this.props.navigation.getParam('back');
                if (back) {
                    this.props.navigation.navigate(back)
                    this.setState({
                        options: [],
                        SelectedOptions: [],
                        api_options: [],
                        isLoading: true,
                    })
                    return true;
                }
                return false;
            });

        })
    }


    async GetData() {
        this.setState({
            options: [],
            SelectedOptions: [],
            api_options: [],
        })
        let GET_DATA = await Model.GET_DATA();
        if (GET_DATA) {
            GET_DATA.data.map((item) => {
                this.state.options.push({ 'value': item.id, 'label': item.name })
            })
            this.setState({
                isLoading: false
            })
        }
    }


    onSelectionsChange = (SelectedOptions) => {
        this.setState({ SelectedOptions })
        console.log('SelectedOptions', SelectedOptions);

    }

    async Submit() {
        this.setState({
            loading: true
        })
        this.state.SelectedOptions.map((item) => {
            this.state.api_options.push(item.value)
        })
        let GET_DATA = await Model.Submit(this.state);
        if (GET_DATA) {
            this.props.navigation.navigate('CommitteesMembers', { DATA: GET_DATA.data, back: 'Filter' })
            this.setState({
                options: [],
                SelectedOptions: [],
                api_options: [],
                isLoading: true,
                loading: false
            })
        }
    }

    render() {
        let back = this.props.navigation.getParam('back');
        if (this.state.isLoading) {
            return (
                <View>
                    <View style={{
                        height: height, width: width, justifyContent: 'center',
                        alignSelf: 'center', backgroundColor: '#fff'
                    }}>
                    </View>
                    <View style={{
                        position: 'absolute',
                        backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
                        borderRadius: 10, justifyContent: 'center', height: 80, width: 80
                    }}>
                        <ActivityIndicator size="large" color="#FFF" style={{
                            alignSelf: 'center',
                        }} />
                    </View>
                </View >
            )
        } else {
            return (
                <Container style={{ backgroundColor: "#fff" }}>
                    {this.Loading()}
                    {/* **********************HEEADER**********************  */}
                    <View style={{
                        backgroundColor: '#de8f4c',
                        flexDirection: 'row-reverse',
                        height: height / 12,
                        width: width,
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
                                backgroundColor: 'transparent',
                                height: '100%',
                                width: '100%',
                                justifyContent: 'center',
                            }}  >
                                <Image source={require('../../images/menu.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Left>
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 5, fontFamily: 'Cairo-Bold' }}>{'فلتر'}</Text>
                        <Right style={{ flex: 1 }}>
                            <Button transparent
                                onPress={() => {
                                    this.props.navigation.navigate(back)
                                    this.setState({
                                        options: [],
                                        SelectedOptions: [],
                                        api_options: [],
                                        isLoading: true,
                                    })
                                }}
                                style={{
                                    backgroundColor: 'transparent',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}  >
                                <Image source={require('../../images/iconsBack.png')} style={{
                                    resizeMode: 'contain',
                                    width: 22,
                                    height: 22
                                }} />
                            </Button>
                        </Right>
                    </View>
                    {/* **********************HEEADER**********************  */}



                    <SelectMultiple
                        style={{ width: '100%', alignSelf: 'center' }}
                        checkboxStyle={{ tintColor: '#b35900', marginLeft: '90%' }}
                        selectedCheckboxStyle={{ tintColor: '#b35900' }}
                        items={this.state.options}
                        labelStyle={{
                            color: '#000', position: 'absolute', left: '25%', textAlign: 'center',
                            fontFamily: 'Cairo-Regular', fontSize: 12, alignSelf: 'center'
                        }}
                        selectedLabelStyle={{
                            color: '#000', position: 'absolute', left: '25%', textAlign: 'center',
                            fontFamily: 'Cairo-Regular', fontSize: 12, alignSelf: 'center'
                        }}
                        selectedItems={this.state.SelectedOptions}
                        onSelectionsChange={this.onSelectionsChange}
                    />


                    <Footer transparent style={{ backgroundColor: '#fff' }}>
                        <Button onPress={() => this.Submit()} block style={{
                            alignSelf: "center", width: "100%", borderRadius: 2,
                            justifyContent: "center", height: '100%',
                            backgroundColor: "#DE8E4C"
                        }}>
                            <Text style={{ textAlign: "center", color: '#fff', fontSize: 16, fontFamily: "Cairo-Bold" }}>تم</Text>
                        </Button>
                    </Footer>
                </Container >
            );
        }
    }
    Loading() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size={60} color="#de8f4c" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 1000
                }} />
            )
        }
    }
}
