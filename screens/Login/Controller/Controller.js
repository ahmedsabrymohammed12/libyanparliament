import Model from '../Model/Model'
import Toast from 'react-native-root-toast';
import { AsyncStorage } from 'react-native';
import { strings } from '../../i18n'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

export default class Controller {

    /////////////////////////////////////////////////// ** Function To Submit Form Data**/////////////////////////////////////////////////////////////////////
    static async Submit(DATA) {
        let responseArray = []
        if (DATA.email == '') {
            // responseArray.push({ 'key': 'email', 'value': strings('lang.Please_insert_email') })
            responseArray.push({ 'key': 'email', 'value': 'برجاء إدخال البريد الإلكترونى' })
        }
        if (DATA.password == '') {
            responseArray.push({ 'key': 'password', 'value': 'برجاء إدخال كلمة السر' })
        }
        else if (DATA.password.length < 6) {
            // Toast.show('كلمة السر يجب أن تكون أكبر من 6 أحرف', { duration: Toast.durations.LONG, position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0 })
        }
        else {
            let LoginResponse = await Model.Register(DATA)
            console.log('LoginResponse', LoginResponse);

            if (LoginResponse.errors.length !== 0) {
                Toast.show(LoginResponse.message, {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0,
                    backgroundColor: "#ffb300", textColor: "#000", fontFamily: "Cairo-Regular"
                })
                responseArray.push({ 'key': 'error', 'value': 'error' })

            }
            else if (LoginResponse.data.accessToken) {
                console.log('accessToken', LoginResponse.data.accessToken);
                Toast.show(LoginResponse.message, {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0,
                    backgroundColor: "#ffb300", textColor: "#000", fontFamily: "Cairo-Regular"
                })
                responseArray.push({ 'key': 'success', 'value': 'success' })
                AsyncStorage.setItem("token", LoginResponse.data.accessToken);
            }
        }
        return responseArray;
    }

    static updateValue(text, field) {
        if (field == 'password') {
            return [text, field]
        }
        if (field == 'email') {
            return [text, field]
        }
    }


    static _signIn = async (token) => {
        try {
            let responseArray = []
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            // this.setState({ userInfo: userInfo, loggedIn: true });
            let DATA = {
                first_name: userInfo.user.givenName,
                last_name: userInfo.user.familyName,
                email: userInfo.user.email,
                social_id: userInfo.user.id,
                token: token
            }
            console.log('userInfo', userInfo);
            console.log('familyName userInfo', userInfo.user.familyName);
            console.log('givenName userInfo', userInfo.user.givenName);
            let LoginResponse = await Model.LoginSocial(DATA)
            if (LoginResponse.token) {
                Toast.show(strings('lang.Login_Successfuly'), {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0,
                    backgroundColor: "#ffb300", textColor: "#000", fontFamily: "Cairo-Regular"
                })
                responseArray.push({ 'key': 'success', 'value': 'success' })
                AsyncStorage.setItem("token", LoginResponse.token);
            } else {
                Toast.show(LoginResponse.error, {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.TOP, shadow: true, animation: true, hideOnPress: true, delay: 0,
                    backgroundColor: "red", textColor: "#fff", fontFamily: "Cairo-Regular"
                })
            }
            return responseArray
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('_signIn error.code 1', error.code);
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('_signIn error.code 2 ', error.code);
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log('_signIn error.code 3', error.code);
                // play services not available or outdated
            } else {
                console.log('_signIn else', error);
                // some other error happened
            }
        }
    };

}
