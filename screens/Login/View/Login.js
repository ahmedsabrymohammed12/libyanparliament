import React, { Component } from 'react';
import {
    View, Image, Text, ActivityIndicator, ImageBackground, Dimensions, StatusBar, BackHandler, AsyncStorage,
    TouchableWithoutFeedback
} from 'react-native';
import { Icon, Content, Header, Input, Button, Left, Right, Container } from "native-base";
const { width, height } = Dimensions.get('window');
import CommonController from '../../Common/Controller'
const public_url = CommonController.getPublicUrl()
import { strings } from '../../i18n'
import Controller from '../Controller/Controller'
import { ScrollView } from 'react-native-gesture-handler';
import { LoginManager, LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import Toast from 'react-native-root-toast';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ////////////////////Form////////////////
            email: '',
            password: '',
            fcmToken: '',
            device: '',
            lan: "",
            social_id: 0,
            ////////////////////errors////////////////
            error_email: '',
            error_password: '',
            loading: false,

            valid: false,
            type: '',
            value: 0,
            social_type: ''
        }
    }

    async componentWillMount() {

        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            BackHandler.addEventListener('hardwareBackPress', () => {
                BackHandler.exitApp()
                return true;
            });
        })
    }

    /////////////////////////////////////////////////// ** Send Data From Form To Controller And Set it in State **/////////////////////////////////////////////////////////////////////
    updateValue(text, field) {
        let return_text = Controller.updateValue(text, field)[0]
        let return_field = Controller.updateValue(text, field)[1]
        if (return_field == 'password') {
            this.setState({
                password: return_text,
                error_password: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_password: 'برجاء إدخال كلمة السر'
                })
            } else if (return_text.length < 6) {
                this.setState({
                    error_password: 'كلمة السر يجب ألا تقل عن 6 أحرف'
                })
            }
        }
        if (return_field == 'email') {
            this.setState({
                email: return_text,
                error_email: ''
            })
            if (return_text.length == 0) {
                this.setState({
                    error_email: ('برجاء إدخال البريد الإلكترونى')
                })
            }
        }
    }
    /////////////////////////////////////////////////// ** login **/////////////////////////////////////////////////////////////////////
    async Submit() {
        this.setState({ loading: true })
        let response = await Controller.Submit(this.state)
        if (response) {
            response.map((item) => {
                if (item.key == 'error') {
                    this.setState({
                        loading: false
                    })
                } if (item.key == 'email') {
                    this.setState({
                        error_email: item.value,
                        loading: false
                    })
                } if (item.key == 'password') {
                    this.setState({
                        error_password: item.value,
                        loading: false
                    })
                } if (item.key == 'success') {
                    this.props.navigation.navigate('Tasks')
                    this.setState({
                        email: '',
                        password: '',
                    })
                    this.setState({ loading: false })
                }
            })
        }
    }



    async componentDidMount() {
        let lan = await AsyncStorage.getItem('lan')
        this.setState({ name: '', email: "", password: "", lan: lan })
    }


    render() {
        return (
            <ScrollView>
                <Container style={{
                }}>
                    <View transparent style={{
                        height: height / 15, width: width,
                        flexDirection: 'row'
                    }}>
                        <StatusBar backgroundColor="#de8f4c" />

                    </View>
                    {this.Loading()}
                    <Content contentContainerStyle={{
                        marginHorizontal: 15,
                        justifyContent: 'center',
                    }}>
                        <View style={{
                            alignContent: 'center',
                            marginTop: 40
                        }}>
                            <Image source={require('../../images/logo.png')} style={{
                                width: width - 20, alignSelf: 'center',
                                height: height / 4, resizeMode: 'contain'
                            }} />
                        </View>

                        <View style={{
                            marginTop: 20,
                            marginBottom: 5
                        }}>
                            <View style={{
                                borderWidth: 2, borderColor: '#de8f4c', borderRadius: 20, marginHorizontal: 20, marginTop: 15,
                                flexDirection: 'row'
                            }}>
                                <Input value={this.state.email} onChangeText={(text) => this.updateValue(text, 'email')}
                                    placeholder={'البريد الإلكترونى'} placeholderTextColor="#de8f4c"
                                    style={{
                                        textAlign: 'right', marginHorizontal: 10, width: '100%', height: '100%', fontSize: 14,
                                        fontFamily: "Cairo-Regular", color: "#de8f4c"
                                    }}>
                                </Input>
                            </View>

                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 35, fontSize: 12, color: 'red' }}>{this.state.error_email}</Text>
                            <View style={{
                                borderWidth: 2, borderColor: '#de8f4c', borderRadius: 20, marginHorizontal: 20, marginTop: 0,
                                flexDirection: 'row'
                            }}>
                                <Input value={this.state.password} secureTextEntry={true} onChangeText={(text) => this.updateValue(text, 'password')}
                                    placeholder={'كلمة السر'} placeholderTextColor="#de8f4c"
                                    style={{
                                        textAlign: 'right', marginHorizontal: 10, width: '100%', height: '100%', fontSize: 14,
                                        fontFamily: "Cairo-Regular", color: "#de8f4c"
                                    }}>
                                </Input>
                            </View>

                            <Text style={{ fontFamily: "Cairo-Regular", marginRight: 35, fontSize: 12, color: 'red' }}>{this.state.error_password}</Text>
                            <Button onPress={() => this.Submit()} style={{
                                borderRadius: 20,
                                marginTop: 0, width: '90%', marginTop: 10, alignSelf: 'center',
                                marginHorizontal: 5,
                                backgroundColor: '#de8f4c',
                                justifyContent: 'center'
                            }} >
                                <Text style={{
                                    color: '#fff', textAlign: 'center',
                                    fontSize: 16, fontFamily: "Cairo-Bold"
                                }}>{'تسجيل الدخول'}</Text>
                            </Button>

                        </View>

                    </Content>
                </Container>
            </ScrollView>
        );
    }

    Loading() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size="large" color="#ffcd57" style={{
                    alignSelf: 'center', position: 'absolute', top: height / 2, zIndex: 1000
                }} />
            )
        }
    }

}
