import React, { Component } from 'react';
import { Container, Content, Header, Text, Button, Left, Right, Textarea, Footer, Card } from "native-base";
import { ScrollView, Dimensions, Modal, Linking, ActivityIndicator, AsyncStorage, Image, StatusBar, View, Animated, BackHandler, TouchableWithoutFeedback } from 'react-native';
const { width, height } = Dimensions.get('window');
import Model from '../Model/Model'
import Controller from '../Controller/Controller';

export default class Decisions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DATA: [],
      page: 1,
      last_page: 0,
      LoadMore: false,
      scrollY: new Animated.Value(0),
      isLoading: true
    }
  }

  componentWillMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {

      this.componentDidMount()
      BackHandler.addEventListener('hardwareBackPress', () => {
        let back = this.props.navigation.getParam('back');
        if (back) {
          this.props.navigation.navigate(back)
          this.setState({
            isLoading: true,
            page: 1,
            DATA: [],
            last_page: 1
          })
          return true;
        }
        return false;
      });

    })
  }

  async LoadMore() {
    if (this.state.last_page > 1) {
      console.log('this.state.last_page', this.state.last_page);
      if (this.state.page < this.state.last_page) {
        console.log('this.state.page before', this.state.page);
        this.setState({
          LoadMore: true,
          page: this.state.page + 1
        })
        console.log('this.state.page after', this.state.page);
        let GET_DATA = await Model.GET_DATA(this.state.page + 1);
        if (GET_DATA) {
          this.setState({
            DATA: [...this.state.DATA, ...GET_DATA.data],
            LoadMore: false,
            last_page: GET_DATA.last_page
          })
        }
      } else {
      }
    } else {
    }
  }


  async componentDidMount() {
    let GET_DATA = await Model.GET_DATA(1);
    if (GET_DATA) {
      this.setState({
        DATA: GET_DATA.data,
        isLoading: false,
        page: 1,
        last_page: GET_DATA.last_page
      })
    }
  }




  renderHome() {
    if (this.state.isLoading) {
      return (
        <View>
          <View style={{
            height: height, width: width, justifyContent: 'center',
            alignSelf: 'center', backgroundColor: '#fff'
          }}>
          </View>
          <View style={{
            position: 'absolute',
            backgroundColor: '#DE8E4C', alignSelf: 'center', marginTop: height / 2 - 50,
            borderRadius: 10, justifyContent: 'center', height: 80, width: 80
          }}>
            <ActivityIndicator size="large" color="#FFF" style={{
              alignSelf: 'center',
            }} />
          </View>
        </View >
      )
    } else {
      return (
        <Content showsVerticalScrollIndicator={false} contentContainerStyle={{
          backgroundColor: '#fff'
        }}
          onScroll={
            Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
              {
                listener: event => {
                  if (Controller.isCloseToBottom(event.nativeEvent)) {
                    this.LoadMore()
                  }
                }
              }
            )
          }>

          <View style={{ width: '95%', backgroundColor: '#f6f6f6', alignSelf: 'center', marginTop: 5 }}>
            {this.state.DATA.map((item, key) =>
              <Card key={key} style={{
                width: width - 30, flexDirection: 'row-reverse', justifyContent: 'center',
                alignItems: "center", backgroundColor: '#fff'
              }}>
                <View style={{ flex: 3 }}>

                  <Text numberOfLines={3} style={{
                    color: '#0f0f0f', textAlign: 'right', marginHorizontal: 10, marginTop: 10,
                    fontSize: 14, fontFamily: 'Cairo-Bold'
                  }}>قرار رقم {item.number}</Text>

                  <Text numberOfLines={3} style={{
                    color: '#0f0f0f', textAlign: 'right', marginHorizontal: 10, marginVertical: 5,
                    fontSize: 14, fontFamily: 'Cairo-Bold'
                  }}>{item.title}</Text>

                  <Text numberOfLines={4} style={{
                    color: '#0f0f0f', textAlign: 'right', marginHorizontal: 10, marginVertical: 0, marginBottom: 10,
                    fontSize: 14, fontFamily: 'Cairo-Regular'
                  }}>{item.body}</Text>
                </View>

                <Button onPress={() => {
                  Linking.canOpenURL(item.file ? item.file : 'test.com').then(supported => {
                    if (supported) {
                      Linking.openURL(item.file);
                    } else {
                    }
                  })
                }} style={{
                  backgroundColor: '#de8f4c', marginHorizontal: 10, borderRadius: 6, marginVertical: 10, flex: 1.5, height: 40,
                  justifyContent: 'center', alignItems: 'center'
                }}>
                  <Text style={{
                    color: '#fff', alignSelf: 'center', textAlign: 'center', fontSize: 14,
                    fontFamily: 'Cairo-Bold'
                  }}>{'تحميل'}</Text>
                  <Image source={require('../../images/download.png')} style={{
                    resizeMode: 'contain', alignSelf: 'center',
                    width: 20, marginRight: 15,
                    height: 20
                  }} />
                </Button>
              </Card>
            )}

          </View>

        </Content >
      )
    }
  }

  render() {
    let back = this.props.navigation.getParam('back');
    return (
      <Container style={{
        justifyContent: 'center',
        alignContent: 'center',
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
      }}>

        <View style={{
          backgroundColor: '#de8f4c',
          flexDirection: 'row-reverse',
          height: height / 12,
          width: width,
        }}>
          <StatusBar backgroundColor="#de8f4c" />
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.toggleDrawer()} style={{
              backgroundColor: 'transparent',
              height: '100%',
              width: '100%',
              justifyContent: 'center',
            }}  >
              <Image source={require('../../images/menu.png')} style={{
                resizeMode: 'contain',
                width: 22,
                height: 22
              }} />
            </Button>
          </Left>
          <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16, flex: 3, fontFamily: 'Cairo-Bold' }}>{'القرارات'}</Text>
          <Right style={{ flex: 2, marginLeft: 10, flexDirection: 'row' }}>
            <Button transparent
              onPress={() => {
                this.props.navigation.navigate(back)
                this.setState({
                  isLoading: true,
                  page: 1,
                  DATA: [],
                  last_page: 0
                })
              }
              }
              style={{
                backgroundColor: 'transparent',
                height: 25,
                width: 25,
                justifyContent: 'center',
              }}  >
              <Image source={require('../../images/iconsBack.png')} style={{
                width: "100%",
                height: "100%"
              }} />
            </Button>
          </Right>
        </View>

        {this.renderHome()}
        {this.Loading()}
      </Container >
    );
  }

  Loading() {
    if (this.state.LoadMore) {
      return (
        <ActivityIndicator size={60} color="#de8f4c" style={{
          alignSelf: 'center', position: 'absolute', top: height / 2 - 50, zIndex: 100
        }} />
      )
    }
  }


}
