import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
// import { gestureHandlerRootHOC } from 'react-native-gesture-handler' 
import HomeScreen from './screens/HomeScreen/View/HomeScreen'
import ListScreen from './screens/ListScreen/View/ListScreen'
import VideosScreen from './screens/VideosScreen/View/VideosScreen'
import ListDetails from './screens/ListDetails/View/ListDetails'
import ContactUs from './screens/ContactUs/View/ContactUs'
import Filter from './screens/Filter/View/Filter'
import MenuDrawer from './screens/components/MenuDrawer';
import Splash from './screens/Splash/View/Splash'
import ParliamentMembers from './screens/ParliamentMembers/View/ParliamentMembers'
import Gallery from './screens/Gallery/View/Gallery'
import GalleryDetails from './screens/GalleryDetails/View/GalleryDetails'
import Decisions from './screens/Decisions/View/Decisions'
import CommitteesMembers from './screens/CommitteesMembers/View/CommitteesMembers'
import ParliamentMemberProfile from './screens/ParliamentMemberProfile/View/ParliamentMemberProfile'
import Tasks from './screens/Tasks/View/Tasks'
import AddTask from './screens/AddTask/View/AddTask'
import Login from './screens/Login/View/Login'
import firebase from 'react-native-firebase';

import { I18nManager, Dimensions } from 'react-native';
const WIDTH = Dimensions.get('window').width;
const DrawerConfig = {
  drawerWidth: WIDTH ,
  // drawerPosition: 'left',
  drawerPosition: 'right',
  drawerLockMode: 'locked-closed',
  contentComponent: ({ navigation }) => {
    // console.warn(navigation.state.params.lang);
    return (<MenuDrawer navigation={navigation} />)
  }
}
const TopLevelNavigator = createDrawerNavigator({
  /////////////////////////////////////////
  Splash: { screen: Splash },
  CommitteesMembers: { screen: CommitteesMembers },
  AddTask: { screen: AddTask },
  Tasks: { screen: Tasks },
  Login: { screen: Login },
  ParliamentMemberProfile: { screen: ParliamentMemberProfile },
  Decisions: { screen: Decisions },
  GalleryDetails: { screen: GalleryDetails },
  Gallery: { screen: Gallery },
  ParliamentMembers: { screen: ParliamentMembers },
  ListScreen: { screen: ListScreen },
  ListDetails: { screen: ListDetails },
  Filter: { screen: Filter },
  ContactUs: { screen: ContactUs },
  VideosScreen: { screen: VideosScreen },
  HomeScreen: { screen: HomeScreen },
},
  DrawerConfig
)
// const AppContainer = createAppContainer(TopLevelNavigator);

const AppContainer = createAppContainer(TopLevelNavigator);

export default class App extends Component {

  componentDidMount() {

    console.log('firebase', firebase);

    this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
      console.log('notification1', notification);
      alert(notification.Notification._body)

    });
    this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {
      // Process your notification as required
      console.log('notification2', notification);
      if (notification.body) {
        alert(notification.body)
      }


    });
  }

  componentWillUnmount() {
    this.removeNotificationDisplayedListener();
    this.removeNotificationListener();
  }


  render() {
    return (
      <AppContainer

      />
    );
  }
}