package com.LibyanParliament;

import android.app.Application; 
import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage; 
import com.reactnativecommunity.viewpager.RNCViewPagerPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;

import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import org.reactnative.camera.RNCameraPackage; 
import com.reactnativecommunity.rnpermissions.RNPermissionsPackage; 
import io.invertase.firebase.RNFirebasePackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; 
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import java.util.Arrays;
import java.util.List;
import com.facebook.react.modules.i18nmanager.I18nUtil;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFetchBlobPackage(),
            new DocumentPickerPackage(), 
                new RNCWebViewPackage(),
             new ReactNativeRestartPackage(),
            new RNCViewPagerPackage(),
            new RNI18nPackage(),
            new AndroidOpenSettingsPackage(),
            new RNCameraPackage(),
            new RNPermissionsPackage(), 
            new RNFirebasePackage(),
            new RNFirebaseNotificationsPackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(),
            new VectorIconsPackage(),
            new RNGestureHandlerPackage(),
            new ReanimatedPackage(),
            new RNFirebaseMessagingPackage(),
            new MapsPackage()
      );
    }
 

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    I18nUtil sharedI18nUtilInstance = I18nUtil.getInstance();
    // sharedI18nUtilInstance.forceRTL(this,true);
    sharedI18nUtilInstance.allowRTL(this, true);
  }
  
}
